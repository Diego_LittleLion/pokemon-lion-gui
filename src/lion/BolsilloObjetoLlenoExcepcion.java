package lion;

public class BolsilloObjetoLlenoExcepcion extends Exception{
	private int cantidad;
	public BolsilloObjetoLlenoExcepcion (int cantidad) {
		this.cantidad = cantidad;
	}
	
	@Override
    public String toString () {
        return "Bolsillo lleno! No puedes guardar más objetos." 
        		+ "\n" + "El tamaño máximo del bolsillo de objetos es de "
        		+ (cantidad - 1) + ". \n";
    }
}
