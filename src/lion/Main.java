package lion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.Toolkit;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import funciones.Fecha;
import funciones.OcultarMostrarPaneles;

import java.awt.SystemColor;
import javax.swing.JComboBox;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import pokemon.*;

/**
 * Esta es la funcion Main, aquí se declaran todos los elementos de la interfaz
 * y sus funciones
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Main {

	private JFrame frmPokemonLion;
	private JTextField nombreJugadorTF;
	private JTextField diaTF;
	private JTextField mesTF;
	private JTextField anioTF;

	/* Variables del personaje y objetos */
	private Personaje persona;
	private Objetos pokeball = new Pokeball();

	/* Instanciamos los pokemons */
	Enemigo enemigo;
	Caterpie cat;
	Lickitung lick;
	Pidgey pid;
	Pikachu pika;
	Rayquaza ray;
	Squirtle squirt;

	/* Variables de Pokemons */
	int pokemonElegido;
	String especie;
	float posibilidadCaptura;
	int nivel;
	int especieRandom;
	JTextField ingresarMoteJF;

	/* Variables del personaje */
	int x = 684;
	int y = 218;
	int pasosDados = 0;
	int pasosRandom;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmPokemonLion.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() throws InterruptedException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings("rawtypes")
	private void initialize() throws InterruptedException {

		/* Preparamos los audios */
		try {
			URL url = this.getClass().getClassLoader().getResource("audios/musica.wav");
			AudioInputStream audioFondo = AudioSystem.getAudioInputStream(url);
			Clip audioFondoClip = AudioSystem.getClip();
			audioFondoClip.open(audioFondo);
			audioFondoClip.loop(Clip.LOOP_CONTINUOUSLY);
			Thread.sleep(100);
		} catch (Exception e) {
		}
		frmPokemonLion = new JFrame();
		frmPokemonLion.setUndecorated(true);
		frmPokemonLion
				.setIconImage(Toolkit.getDefaultToolkit().getImage(Main.class.getResource("/imagenes/iconoapp.png")));
		frmPokemonLion.setTitle("Pokemon Lion");
		frmPokemonLion.setBounds(0, 0, 1000, 600);
		frmPokemonLion.setLocationRelativeTo(null);
		frmPokemonLion.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPokemonLion.setResizable(false);
		frmPokemonLion.getContentPane().setLayout(null);

		/* Creamos un grupo para los radio de los generos */
		ButtonGroup group = new ButtonGroup();

		JPanel panelPrincipal = new JPanel();
		panelPrincipal.setBounds(0, 0, 1000, 600);
		frmPokemonLion.getContentPane().add(panelPrincipal);
		panelPrincipal.setLayout(null);

		JLabel salirBoton = new JLabel();
		salirBoton.setIcon(new ImageIcon(Main.class.getResource("/imagenes/salir.png")));
		salirBoton.setHorizontalAlignment(SwingConstants.CENTER);
		salirBoton.setBounds(929, 11, 50, 50);
		panelPrincipal.add(salirBoton);

		JPanel menuPrincipal = new JPanel();
		menuPrincipal.setName("menuPrincipal");
		menuPrincipal.setOpaque(false);
		menuPrincipal.setBounds(0, 132, 1000, 468);
		menuPrincipal.setVisible(false);

		JLabel bordeVentana = new JLabel("");
		bordeVentana.setBounds(0, 0, 1000, 600);
		panelPrincipal.add(bordeVentana);
		bordeVentana.setIcon(new ImageIcon(Main.class.getResource("/imagenes/bordeVentana.png")));

		JLabel bienvenidaLabel = new JLabel();
		bienvenidaLabel.setBounds(0, 10, 1000, 111);
		panelPrincipal.add(bienvenidaLabel);
		bienvenidaLabel.setIcon(new ImageIcon(Main.class.getResource("/imagenes/titulo.png")));
		bienvenidaLabel.setFont(new Font("Calibri", Font.BOLD, 34));
		bienvenidaLabel.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel panelCombate = new JPanel();
		panelCombate.setOpaque(false);
		panelCombate.setBounds(0, 132, 1000, 468);
		panelPrincipal.add(panelCombate);
		panelCombate.setLayout(null);

		JLabel botonHuir = new JLabel("");
		botonHuir.setBounds(421, 408, 120, 35);
		panelCombate.add(botonHuir);
		botonHuir.setIcon(new ImageIcon(Main.class.getResource("/imagenes/huirBoton.png")));
		botonHuir.setFont(new Font("Tahoma", Font.BOLD, 15));
		botonHuir.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel panelLucha = new JPanel();
		panelLucha.setBounds(0, 0, 1000, 468);
		panelCombate.add(panelLucha);
		panelLucha.setVisible(false);
		panelLucha.setLayout(null);

		JLabel imgPokemonEnemigo = new JLabel("");
		imgPokemonEnemigo.setIcon(new ImageIcon(Main.class.getResource("/imagenesPokemon/Lickitung.png")));
		imgPokemonEnemigo.setBounds(790, 11, 200, 200);
		panelLucha.add(imgPokemonEnemigo);

		JLabel imgPokemonAmigo = new JLabel("");
		imgPokemonAmigo.setIcon(new ImageIcon(Main.class.getResource("/imagenesPokemon/Lickitung.png")));
		imgPokemonAmigo.setHorizontalAlignment(SwingConstants.CENTER);
		imgPokemonAmigo.setBounds(10, 11, 200, 200);
		panelLucha.add(imgPokemonAmigo);

		JButton ataque1Boton = new JButton("");
		ataque1Boton.setBounds(257, 244, 190, 23);
		panelLucha.add(ataque1Boton);

		JButton ataque2Boton = new JButton("");
		ataque2Boton.setBounds(514, 244, 190, 23);
		panelLucha.add(ataque2Boton);

		JButton ataque3Boton = new JButton("");
		ataque3Boton.setBounds(257, 335, 190, 23);
		panelLucha.add(ataque3Boton);

		JButton ataque4Boton = new JButton("");
		ataque4Boton.setBounds(514, 335, 190, 23);
		panelLucha.add(ataque4Boton);

		JLabel vidaPokemonAliado = new JLabel("");
		vidaPokemonAliado.setHorizontalAlignment(SwingConstants.CENTER);
		vidaPokemonAliado.setBounds(10, 244, 200, 14);
		panelLucha.add(vidaPokemonAliado);

		JLabel nivelPokemonAliado = new JLabel("");
		nivelPokemonAliado.setHorizontalAlignment(SwingConstants.CENTER);
		nivelPokemonAliado.setBounds(10, 269, 200, 14);
		panelLucha.add(nivelPokemonAliado);

		JLabel vidaPokemonEnemigo = new JLabel("");
		vidaPokemonEnemigo.setHorizontalAlignment(SwingConstants.CENTER);
		vidaPokemonEnemigo.setBounds(778, 244, 200, 14);
		panelLucha.add(vidaPokemonEnemigo);

		JLabel nivelPokemonEnemigo = new JLabel("");
		nivelPokemonEnemigo.setHorizontalAlignment(SwingConstants.CENTER);
		nivelPokemonEnemigo.setBounds(778, 269, 200, 14);
		panelLucha.add(nivelPokemonEnemigo);

		JLabel danioGenerado = new JLabel("");
		danioGenerado.setHorizontalAlignment(SwingConstants.CENTER);
		danioGenerado.setBounds(220, 140, 560, 14);
		panelLucha.add(danioGenerado);

		JLabel ataqueUsado = new JLabel("");
		ataqueUsado.setHorizontalAlignment(SwingConstants.CENTER);
		ataqueUsado.setBounds(220, 78, 560, 14);
		panelLucha.add(ataqueUsado);

		JPanel panelSeleccionPokemon = new JPanel();
		panelSeleccionPokemon.setVisible(false);
		panelSeleccionPokemon.setBounds(0, 0, 1000, 468);
		panelCombate.add(panelSeleccionPokemon);
		panelSeleccionPokemon.setLayout(null);

		JLabel lblSeleccionDePokemon = new JLabel("SELECCION DE POKEMON");
		lblSeleccionDePokemon.setBounds(0, 25, 1015, 34);
		lblSeleccionDePokemon.setHorizontalAlignment(SwingConstants.CENTER);
		lblSeleccionDePokemon.setFont(new Font("Calibri", Font.PLAIN, 27));
		panelSeleccionPokemon.add(lblSeleccionDePokemon);

		JLabel seleccionaUnPokemonJL = new JLabel("Selecciona un Pokemon para combatir contra ");
		seleccionaUnPokemonJL.setHorizontalAlignment(SwingConstants.CENTER);
		seleccionaUnPokemonJL.setBounds(0, 87, 1015, 14);
		panelSeleccionPokemon.add(seleccionaUnPokemonJL);

		JComboBox listaPokemonLuchar = new JComboBox();
		listaPokemonLuchar.setMaximumRowCount(10);
		listaPokemonLuchar.setBounds(289, 138, 471, 20);
		panelSeleccionPokemon.add(listaPokemonLuchar);

		JButton detallesAliadoBoton = new JButton("Detalles");
		detallesAliadoBoton.setBounds(287, 193, 89, 23);
		panelSeleccionPokemon.add(detallesAliadoBoton);

		JButton botonLuchar = new JButton("Luchar");
		botonLuchar.setBounds(672, 193, 89, 23);
		panelSeleccionPokemon.add(botonLuchar);

		JLabel imgPokemonLuchar = new JLabel("");
		imgPokemonLuchar.setHorizontalAlignment(SwingConstants.CENTER);
		imgPokemonLuchar.setBounds(131, 249, 150, 150);
		panelSeleccionPokemon.add(imgPokemonLuchar);

		JLabel nivelPKElegidoLucha = new JLabel("Nivel: ");
		nivelPKElegidoLucha.setVisible(false);
		nivelPKElegidoLucha.setBounds(376, 249, 82, 14);
		panelSeleccionPokemon.add(nivelPKElegidoLucha);

		JLabel tipoPKElegidoLucha = new JLabel("Tipo: ");
		tipoPKElegidoLucha.setVisible(false);
		tipoPKElegidoLucha.setBounds(376, 281, 164, 14);
		panelSeleccionPokemon.add(tipoPKElegidoLucha);

		JLabel psPKElegidoLucha = new JLabel("PS: ");
		psPKElegidoLucha.setVisible(false);
		psPKElegidoLucha.setBounds(376, 315, 164, 14);
		panelSeleccionPokemon.add(psPKElegidoLucha);

		JLabel ataquePKElegidoLucha = new JLabel("Ataque: ");
		ataquePKElegidoLucha.setVisible(false);
		ataquePKElegidoLucha.setBounds(376, 348, 164, 14);
		panelSeleccionPokemon.add(ataquePKElegidoLucha);

		JLabel defensaPKElegidoLucha = new JLabel("Defensa: ");
		defensaPKElegidoLucha.setVisible(false);
		defensaPKElegidoLucha.setBounds(376, 385, 164, 14);
		panelSeleccionPokemon.add(defensaPKElegidoLucha);

		JLabel ataque1PKElegidoLucha = new JLabel("Ataque 1: ");
		ataque1PKElegidoLucha.setVisible(false);
		ataque1PKElegidoLucha.setBounds(638, 249, 266, 14);
		panelSeleccionPokemon.add(ataque1PKElegidoLucha);

		JLabel ataque2PKElegidoLucha = new JLabel("Ataque 2: ");
		ataque2PKElegidoLucha.setVisible(false);
		ataque2PKElegidoLucha.setBounds(638, 281, 266, 14);
		panelSeleccionPokemon.add(ataque2PKElegidoLucha);

		JLabel ataque3PKElegidoLucha = new JLabel("Ataque 3: ");
		ataque3PKElegidoLucha.setVisible(false);
		ataque3PKElegidoLucha.setBounds(638, 315, 266, 14);
		panelSeleccionPokemon.add(ataque3PKElegidoLucha);

		JLabel ataque4PKElegidoLucha = new JLabel("Ataque 4: ");
		ataque4PKElegidoLucha.setVisible(false);
		ataque4PKElegidoLucha.setBounds(638, 348, 266, 14);
		panelSeleccionPokemon.add(ataque4PKElegidoLucha);
		panelCombate.setVisible(false);
		menuPrincipal.setLayout(null);
		panelPrincipal.add(menuPrincipal);

		JPanel informacionPanel = new JPanel();
		informacionPanel.setName("informacionPanel");
		informacionPanel.setVisible(false);
		informacionPanel.setBounds(0, 0, 787, 457);

		JPanel tiendaPanel = new JPanel();
		tiendaPanel.setVisible(false);
		tiendaPanel.setBounds(0, 0, 787, 457);

		JPanel busquedaPanel = new JPanel();
		busquedaPanel.setVisible(false);
		busquedaPanel.setBounds(0, 0, 787, 457);

		JPanel pokemonLiberadoPanel = new JPanel();
		pokemonLiberadoPanel.setVisible(false);
		pokemonLiberadoPanel.setBounds(0, 0, 787, 457);

		JPanel mostrarPokemonsPanel = new JPanel();
		mostrarPokemonsPanel.setVisible(false);
		mostrarPokemonsPanel.setBounds(0, 0, 787, 457);
		menuPrincipal.add(mostrarPokemonsPanel);
		mostrarPokemonsPanel.setLayout(null);

		JLabel tusPokemonJL = new JLabel("TUS POKEMON");
		tusPokemonJL.setHorizontalAlignment(SwingConstants.CENTER);
		tusPokemonJL.setFont(new Font("Calibri", Font.PLAIN, 27));
		tusPokemonJL.setBounds(0, 25, 787, 34);
		mostrarPokemonsPanel.add(tusPokemonJL);

		JComboBox listadoPokemonBOX = new JComboBox();
		listadoPokemonBOX.setMaximumRowCount(10);
		listadoPokemonBOX.setBounds(160, 118, 471, 20);
		mostrarPokemonsPanel.add(listadoPokemonBOX);

		JButton verDetallesBoton = new JButton("Detalles");
		verDetallesBoton.setBounds(45, 336, 89, 23);
		mostrarPokemonsPanel.add(verDetallesBoton);

		JButton liberarBoton = new JButton("Liberar");
		liberarBoton.setBounds(653, 336, 89, 23);
		mostrarPokemonsPanel.add(liberarBoton);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Main.class.getResource("/imagenes/listaPokemon.gif")));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(266, 247, 280, 210);
		mostrarPokemonsPanel.add(lblNewLabel);
		menuPrincipal.add(pokemonLiberadoPanel);
		pokemonLiberadoPanel.setLayout(null);

		JLabel pokemonLiberadoJL = new JLabel("");
		pokemonLiberadoJL.setForeground(new Color(0, 0, 0));
		pokemonLiberadoJL.setFont(new Font("Calibri", Font.BOLD, 42));
		pokemonLiberadoJL.setHorizontalAlignment(SwingConstants.CENTER);
		pokemonLiberadoJL.setBounds(0, 52, 787, 373);
		pokemonLiberadoPanel.add(pokemonLiberadoJL);

		JLabel lblFondo = new JLabel("");
		lblFondo.setHorizontalAlignment(SwingConstants.CENTER);
		lblFondo.setIcon(new ImageIcon(Main.class.getResource("/imagenes/cartelDespedida.png")));
		lblFondo.setBounds(0, 52, 787, 373);
		pokemonLiberadoPanel.add(lblFondo);
		menuPrincipal.add(busquedaPanel);
		busquedaPanel.setLayout(null);

		JPanel terrenoPanel = new JPanel();
		terrenoPanel.setBounds(10, 11, 767, 446);
		busquedaPanel.add(terrenoPanel);
		terrenoPanel.setLayout(null);

		JLabel personaje = new JLabel("");
		personaje.setHorizontalAlignment(SwingConstants.CENTER);
		personaje.setIcon(new ImageIcon(Main.class.getResource("/imagenesPersonaje/personaje.png")));
		personaje.setBounds(684, 218, 47, 94);
		terrenoPanel.add(personaje);

		JLabel terreno = new JLabel("");
		terreno.setBounds(0, 0, 767, 446);
		terrenoPanel.add(terreno);
		terreno.setIcon(new ImageIcon(Main.class.getResource("/imagenes/terreno.png")));

		JPanel capturadoPanel = new JPanel();
		capturadoPanel.setVisible(false);
		capturadoPanel.setBounds(0, 0, 787, 457);
		menuPrincipal.add(capturadoPanel);
		capturadoPanel.setLayout(null);

		JLabel capturadoTitulo = new JLabel("POKEMON CAPTURADO");
		capturadoTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		capturadoTitulo.setBounds(0, 25, 787, 34);
		capturadoTitulo.setFont(new Font("Calibri", Font.PLAIN, 27));
		capturadoPanel.add(capturadoTitulo);

		JLabel especieJL = new JLabel("Especie: ");
		especieJL.setHorizontalAlignment(SwingConstants.LEFT);
		especieJL.setBounds(456, 133, 211, 14);
		capturadoPanel.add(especieJL);

		JLabel nicelJL = new JLabel("Nivel: ");
		nicelJL.setHorizontalAlignment(SwingConstants.LEFT);
		nicelJL.setBounds(456, 160, 211, 14);
		capturadoPanel.add(nicelJL);

		JLabel moteJL = new JLabel("Mote: ");
		moteJL.setHorizontalAlignment(SwingConstants.LEFT);
		moteJL.setBounds(456, 190, 46, 14);
		capturadoPanel.add(moteJL);

		ingresarMoteJF = new JTextField();
		ingresarMoteJF.setBounds(512, 187, 155, 20);
		capturadoPanel.add(ingresarMoteJF);
		ingresarMoteJF.setColumns(10);

		JButton guardarBoton = new JButton("Guardar");
		guardarBoton.setBounds(512, 246, 89, 23);
		capturadoPanel.add(guardarBoton);

		JLabel guardadoJL = new JLabel("");
		guardadoJL.setFont(new Font("Calibri", Font.PLAIN, 15));
		guardadoJL.setHorizontalAlignment(SwingConstants.CENTER);
		guardadoJL.setBounds(376, 338, 350, 14);
		capturadoPanel.add(guardadoJL);

		JLabel fotoPokemonCapturado = new JLabel("");
		fotoPokemonCapturado.setHorizontalAlignment(SwingConstants.CENTER);
		fotoPokemonCapturado.setBounds(66, 98, 300, 300);
		capturadoPanel.add(fotoPokemonCapturado);

		JPanel capturaPanel = new JPanel();
		capturaPanel.setVisible(false);
		capturaPanel.setBounds(0, 0, 787, 457);
		menuPrincipal.add(capturaPanel);
		capturaPanel.setLayout(null);

		JLabel capturaTitulo = new JLabel("HA APARECIDO UN POKEMON!");
		capturaTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		capturaTitulo.setBounds(10, 25, 767, 34);
		capturaTitulo.setFont(new Font("Calibri", Font.PLAIN, 27));
		capturaPanel.add(capturaTitulo);

		JLabel pokemonAparecidoJL = new JLabel("");
		pokemonAparecidoJL.setHorizontalAlignment(SwingConstants.CENTER);
		pokemonAparecidoJL.setBounds(10, 73, 767, 14);
		pokemonAparecidoJL.setVisible(true);
		capturaPanel.add(pokemonAparecidoJL);

		JLabel capturadoJL = new JLabel("");
		capturadoJL.setHorizontalAlignment(SwingConstants.CENTER);
		capturadoJL.setBounds(306, 154, 471, 14);
		capturaPanel.add(capturadoJL);

		JLabel recompensaJL = new JLabel("");
		recompensaJL.setHorizontalAlignment(SwingConstants.CENTER);
		recompensaJL.setBounds(306, 179, 471, 14);
		capturaPanel.add(recompensaJL);

		JLabel escapadoJL = new JLabel("");
		escapadoJL.setHorizontalAlignment(SwingConstants.CENTER);
		escapadoJL.setBounds(306, 154, 471, 14);
		capturaPanel.add(escapadoJL);

		JLabel reintentarJL = new JLabel("");
		reintentarJL.setHorizontalAlignment(SwingConstants.CENTER);
		reintentarJL.setBounds(306, 179, 471, 14);
		capturaPanel.add(reintentarJL);

		JButton capturarPokemonBoton = new JButton("Capturar");
		capturarPokemonBoton.setBounds(487, 313, 104, 23);
		capturarPokemonBoton.setVisible(false);
		capturaPanel.add(capturarPokemonBoton);

		JButton siguienteBoton = new JButton("Siguiente");
		siguienteBoton.setBounds(487, 313, 104, 23);
		siguienteBoton.setVisible(false);
		capturaPanel.add(siguienteBoton);

		JLabel fotoPokemonAparecido = new JLabel("");
		fotoPokemonAparecido.setHorizontalAlignment(SwingConstants.CENTER);
		fotoPokemonAparecido.setBounds(66, 98, 300, 300);
		capturaPanel.add(fotoPokemonAparecido);
		menuPrincipal.add(tiendaPanel);
		tiendaPanel.setLayout(null);

		JLabel lblTienda = new JLabel("TIENDA");
		lblTienda.setHorizontalAlignment(SwingConstants.CENTER);
		lblTienda.setBounds(0, 25, 787, 34);
		lblTienda.setFont(new Font("Calibri", Font.PLAIN, 27));
		tiendaPanel.add(lblTienda);

		JLabel label = new JLabel("\r\n");
		label.setIcon(new ImageIcon(Main.class.getResource("/imagenes/miniPokeball.png")));
		label.setBounds(332, 104, 20, 20);
		tiendaPanel.add(label);

		JButton comprarBoton = new JButton("Comprar");
		comprarBoton.setBounds(348, 326, 89, 23);
		tiendaPanel.add(comprarBoton);

		JComboBox cantidadPokeballsComprar = new JComboBox();
		cantidadPokeballsComprar
				.setModel(new DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
		cantidadPokeballsComprar.setBounds(407, 104, 62, 20);
		tiendaPanel.add(cantidadPokeballsComprar);

		JLabel avisoCompraRealizada = new JLabel("");
		avisoCompraRealizada.setHorizontalAlignment(SwingConstants.CENTER);
		avisoCompraRealizada.setBounds(169, 371, 471, 14);
		tiendaPanel.add(avisoCompraRealizada);

		JLabel fotoTienda = new JLabel("");
		fotoTienda.setIcon(new ImageIcon(Main.class.getResource("/imagenes/tienda.png")));
		fotoTienda.setHorizontalAlignment(SwingConstants.CENTER);
		fotoTienda.setBounds(22, 103, 264, 264);
		tiendaPanel.add(fotoTienda);

		JLabel lblPrecios = new JLabel("<html><u>PRECIOS</u></html>");
		lblPrecios.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPrecios.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrecios.setBounds(605, 107, 97, 31);
		tiendaPanel.add(lblPrecios);

		JLabel label_1 = new JLabel("\r\n");
		label_1.setIcon(new ImageIcon(Main.class.getResource("/imagenes/miniPokeball.png")));
		label_1.setBounds(626, 160, 20, 20);
		tiendaPanel.add(label_1);

		JLabel label_2 = new JLabel("-  100");
		label_2.setBounds(656, 163, 46, 14);
		tiendaPanel.add(label_2);
		menuPrincipal.add(informacionPanel);
		informacionPanel.setLayout(null);

		JLabel iInformacinDelJugadorTitulo = new JLabel("INFORMACI\u00D3N DEL JUGADOR");
		iInformacinDelJugadorTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		iInformacinDelJugadorTitulo.setBounds(0, 25, 787, 34);
		iInformacinDelJugadorTitulo.setFont(new Font("Calibri", Font.PLAIN, 27));
		informacionPanel.add(iInformacinDelJugadorTitulo);

		JLabel mostrarNombreJL = new JLabel("Nombre: ");
		mostrarNombreJL.setBounds(496, 164, 250, 14);
		informacionPanel.add(mostrarNombreJL);

		JLabel mostrarNacimientoJL = new JLabel("Fecha de nacimiento: ");
		mostrarNacimientoJL.setBounds(496, 188, 250, 14);
		informacionPanel.add(mostrarNacimientoJL);

		JLabel mostrarGeneroJL = new JLabel("Genero: ");
		mostrarGeneroJL.setBounds(496, 213, 250, 14);
		informacionPanel.add(mostrarGeneroJL);

		JLabel mostrarPokecoinsJL = new JLabel("PokeCoins: ");
		mostrarPokecoinsJL.setBounds(496, 238, 250, 14);
		informacionPanel.add(mostrarPokecoinsJL);

		JLabel mostrarCantidadObjetosJL = new JLabel("Cantidad de objetos: ");
		mostrarCantidadObjetosJL.setBounds(496, 263, 250, 14);
		informacionPanel.add(mostrarCantidadObjetosJL);

		JLabel mostrarCantidadPokeballsJL = new JLabel("Pokeballs: ");
		mostrarCantidadPokeballsJL.setBounds(496, 288, 250, 14);
		informacionPanel.add(mostrarCantidadPokeballsJL);

		JLabel mostrarCantidadPokemonJL = new JLabel("Cantidad de Pokemon: ");
		mostrarCantidadPokemonJL.setBounds(496, 313, 250, 14);
		informacionPanel.add(mostrarCantidadPokemonJL);

		JLabel fotoJugador = new JLabel("");
		fotoJugador.setHorizontalAlignment(SwingConstants.CENTER);
		fotoJugador.setBounds(10, 82, 476, 318);
		informacionPanel.add(fotoJugador);

		JPanel detallesPokemonPanel = new JPanel();
		detallesPokemonPanel.setVisible(false);
		detallesPokemonPanel.setBounds(0, 0, 787, 457);
		menuPrincipal.add(detallesPokemonPanel);
		detallesPokemonPanel.setLayout(null);

		JLabel nombrePokemonElegidoJL = new JLabel("INFORMACION");
		nombrePokemonElegidoJL.setBounds(0, 25, 787, 34);
		nombrePokemonElegidoJL.setHorizontalAlignment(SwingConstants.CENTER);
		nombrePokemonElegidoJL.setFont(new Font("Calibri", Font.PLAIN, 27));
		detallesPokemonPanel.add(nombrePokemonElegidoJL);

		JLabel fotoPokemonInformacion = new JLabel("");
		fotoPokemonInformacion.setBounds(10, 65, 381, 381);
		detallesPokemonPanel.add(fotoPokemonInformacion);
		fotoPokemonInformacion.setHorizontalAlignment(SwingConstants.CENTER);

		JLabel nivelEspecieJL = new JLabel("Nivel: ");
		nivelEspecieJL.setBounds(446, 125, 164, 14);
		detallesPokemonPanel.add(nivelEspecieJL);

		JLabel tipoEspecieJL = new JLabel("Tipo: ");
		tipoEspecieJL.setBounds(446, 149, 164, 14);
		detallesPokemonPanel.add(tipoEspecieJL);

		JLabel psEspecieJL = new JLabel("PS: ");
		psEspecieJL.setBounds(446, 174, 164, 14);
		detallesPokemonPanel.add(psEspecieJL);

		JLabel ataqueEspecieJL = new JLabel("Ataque: ");
		ataqueEspecieJL.setBounds(446, 199, 164, 14);
		detallesPokemonPanel.add(ataqueEspecieJL);

		JLabel defensaEspecieJL = new JLabel("Defensa: ");
		defensaEspecieJL.setBounds(446, 224, 164, 14);
		detallesPokemonPanel.add(defensaEspecieJL);

		JLabel ataque1JL = new JLabel("Ataque 1: ");
		ataque1JL.setBounds(446, 248, 266, 14);
		detallesPokemonPanel.add(ataque1JL);

		JLabel ataque2JL = new JLabel("Ataque 2: ");
		ataque2JL.setBounds(446, 272, 266, 14);
		detallesPokemonPanel.add(ataque2JL);

		JLabel ataque3JL = new JLabel("Ataque 3: ");
		ataque3JL.setBounds(446, 295, 266, 14);
		detallesPokemonPanel.add(ataque3JL);

		JLabel ataque4JL = new JLabel("Ataque 4: ");
		ataque4JL.setBounds(446, 317, 266, 14);
		detallesPokemonPanel.add(ataque4JL);

		JLabel capturadoEspecieJL = new JLabel("Capturado: ");
		capturadoEspecieJL.setBounds(446, 338, 266, 14);
		detallesPokemonPanel.add(capturadoEspecieJL);

		JPanel opcionesPanel = new JPanel();
		opcionesPanel.setBounds(786, 11, 193, 446);
		menuPrincipal.add(opcionesPanel);
		opcionesPanel.setLayout(null);

		JLabel menuJL = new JLabel("<html><u>MENÚ</u></html>");
		menuJL.setHorizontalAlignment(SwingConstants.CENTER);
		menuJL.setFont(new Font("Calibri", Font.PLAIN, 25));
		menuJL.setBounds(10, 11, 173, 30);
		opcionesPanel.add(menuJL);

		JButton busquedaBoton = new JButton("Salir de busqueda");
		busquedaBoton.setBounds(10, 64, 173, 23);
		opcionesPanel.add(busquedaBoton);
		busquedaBoton.setFocusable(true);

		JButton listarPokemonBoton = new JButton("Listar Pokemons");
		listarPokemonBoton.setBounds(10, 127, 173, 23);
		opcionesPanel.add(listarPokemonBoton);

		JButton tiendaBoton = new JButton("Tienda");
		tiendaBoton.setBounds(10, 188, 173, 23);
		opcionesPanel.add(tiendaBoton);

		JButton informacionPersonalBoton = new JButton("Informaci\u00F3n personal");
		informacionPersonalBoton.setBounds(10, 245, 173, 23);
		opcionesPanel.add(informacionPersonalBoton);

		JLabel miniPokeball = new JLabel("\r\n");
		miniPokeball.setIcon(new ImageIcon(Main.class.getResource("/imagenes/miniPokeball.png")));
		miniPokeball.setBounds(22, 415, 20, 20);
		opcionesPanel.add(miniPokeball);

		JLabel cantidadPokeballsMini = new JLabel("");
		cantidadPokeballsMini.setBounds(51, 418, 46, 14);
		opcionesPanel.add(cantidadPokeballsMini);

		JLabel miniPokecoin = new JLabel("");
		miniPokecoin.setIcon(new ImageIcon(Main.class.getResource("/imagenes/miniPokecoin.png")));
		miniPokecoin.setBounds(107, 415, 20, 20);
		opcionesPanel.add(miniPokecoin);

		JLabel cantidadCoinsMini = new JLabel("");
		cantidadCoinsMini.setBounds(137, 418, 46, 14);
		opcionesPanel.add(cantidadCoinsMini);

		JPanel menuPrincipalPanel = new JPanel();
		menuPrincipalPanel.setBounds(0, 0, 787, 457);
		menuPrincipal.add(menuPrincipalPanel);

		JLabel menuPrincipalJL = new JLabel();
		menuPrincipalPanel.add(menuPrincipalJL);
		menuPrincipalJL.setIcon(new ImageIcon(Main.class.getResource("/imagenes/Pokeball.gif")));
		menuPrincipalJL.setFont(new Font("Calibri", Font.PLAIN, 40));
		menuPrincipalJL.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel panelBienvenida = new JPanel();
		panelBienvenida.setOpaque(false);
		panelBienvenida.setBounds(0, 132, 1000, 468);
		panelPrincipal.add(panelBienvenida);
		panelBienvenida.setLayout(null);

		JLabel imagenOak = new JLabel("");
		imagenOak.setBounds(720, 11, 280, 456);
		panelBienvenida.add(imagenOak);
		imagenOak.setIcon(new ImageIcon(Main.class.getResource("/imagenes/oak.png")));
		imagenOak.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel formularioDatosJugador = new JPanel();
		formularioDatosJugador.setBounds(0, 0, 787, 400);
		panelBienvenida.add(formularioDatosJugador);
		formularioDatosJugador.setLayout(null);
		formularioDatosJugador.setVisible(false);

		JLabel nombreJugadorJL = new JLabel("Nombre de jugador");
		nombreJugadorJL.setFont(new Font("Calibri", Font.BOLD, 15));
		nombreJugadorJL.setHorizontalAlignment(SwingConstants.CENTER);
		nombreJugadorJL.setBounds(10, 58, 735, 14);
		formularioDatosJugador.add(nombreJugadorJL);

		nombreJugadorTF = new JTextField();
		nombreJugadorTF.setHorizontalAlignment(SwingConstants.CENTER);
		nombreJugadorTF.setBounds(194, 84, 373, 20);
		formularioDatosJugador.add(nombreJugadorTF);
		nombreJugadorTF.setColumns(10);

		JLabel FechaDeNacimientoTITULO = new JLabel("Fecha de nacimiento");
		FechaDeNacimientoTITULO.setHorizontalAlignment(SwingConstants.CENTER);
		FechaDeNacimientoTITULO.setFont(new Font("Calibri", Font.BOLD, 15));
		FechaDeNacimientoTITULO.setBounds(10, 163, 735, 14);
		formularioDatosJugador.add(FechaDeNacimientoTITULO);

		JLabel diaJL = new JLabel("Dia");
		diaJL.setFont(new Font("Calibri", Font.PLAIN, 14));
		diaJL.setHorizontalAlignment(SwingConstants.CENTER);
		diaJL.setBounds(20, 188, 46, 14);
		formularioDatosJugador.add(diaJL);

		diaTF = new JTextField();
		diaTF.setHorizontalAlignment(SwingConstants.CENTER);
		diaTF.setBounds(60, 185, 86, 20);
		formularioDatosJugador.add(diaTF);
		diaTF.setColumns(10);

		JLabel mesJL = new JLabel("Mes");
		mesJL.setFont(new Font("Calibri", Font.PLAIN, 14));
		mesJL.setHorizontalAlignment(SwingConstants.CENTER);
		mesJL.setBounds(309, 188, 46, 14);
		formularioDatosJugador.add(mesJL);

		mesTF = new JTextField();
		mesTF.setHorizontalAlignment(SwingConstants.CENTER);
		mesTF.setBounds(356, 185, 86, 20);
		formularioDatosJugador.add(mesTF);
		mesTF.setColumns(10);

		JLabel anioJL = new JLabel("A\u00F1o");
		anioJL.setFont(new Font("Calibri", Font.PLAIN, 14));
		anioJL.setHorizontalAlignment(SwingConstants.CENTER);
		anioJL.setBounds(603, 188, 46, 14);
		formularioDatosJugador.add(anioJL);

		anioTF = new JTextField();
		anioTF.setHorizontalAlignment(SwingConstants.CENTER);
		anioTF.setBounds(659, 185, 86, 20);
		formularioDatosJugador.add(anioTF);
		anioTF.setColumns(10);

		JLabel generoJL = new JLabel("Genero");
		generoJL.setHorizontalAlignment(SwingConstants.CENTER);
		generoJL.setFont(new Font("Calibri", Font.BOLD, 15));
		generoJL.setBounds(10, 243, 735, 14);
		formularioDatosJugador.add(generoJL);

		JRadioButton chicoRadio = new JRadioButton("Chico");
		chicoRadio.setBounds(40, 269, 109, 23);
		group.add(chicoRadio);
		formularioDatosJugador.add(chicoRadio);

		JRadioButton chicaRadio = new JRadioButton("Chica");
		chicaRadio.setBounds(356, 269, 80, 23);
		group.add(chicaRadio);
		formularioDatosJugador.add(chicaRadio);

		JRadioButton otroRadio = new JRadioButton("Otro");
		otroRadio.setBounds(624, 269, 109, 23);
		group.add(otroRadio);
		formularioDatosJugador.add(otroRadio);

		JButton aceptarBoton = new JButton("Aceptar");
		aceptarBoton.setBounds(326, 361, 127, 23);
		formularioDatosJugador.add(aceptarBoton);

		JLabel mensajeBienvenidaJL = new JLabel("Haz click en 'Continuar'...");
		mensajeBienvenidaJL.setBounds(0, 82, 781, 224);
		panelBienvenida.add(mensajeBienvenidaJL);
		mensajeBienvenidaJL.setHorizontalAlignment(SwingConstants.CENTER);

		JButton continuarBoton = new JButton("Continuar");
		continuarBoton.setBounds(326, 361, 127, 23);
		panelBienvenida.add(continuarBoton);

		continuarBoton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				funciones.Bienvenida.imprimirBienvenida(mensajeBienvenidaJL, panelBienvenida, formularioDatosJugador,
						imagenOak, menuPrincipal, continuarBoton, menuPrincipalPanel);
			}
		});

		aceptarBoton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String genero;
				if (nombreJugadorTF.getText().equals("") && diaTF.getText().equals("") && mesTF.getText().equals("")
						&& anioTF.getText().equals("")) {
					JOptionPane.showMessageDialog(frmPokemonLion, "Rellena los datos del formulario, por favor.");
				} else {
					if (chicoRadio.isSelected()) {
						genero = "Chico";
					} else if (chicaRadio.isSelected()) {
						genero = "Chica";
					} else if (otroRadio.isSelected()) {
						genero = "Otro";
					} else {
						genero = "No indicado";
					}
					if (!Fecha.comprobarfecha(diaTF.getText(), mesTF.getText(), anioTF.getText(), frmPokemonLion)) {
						persona = new Personaje(nombreJugadorTF.getText(), diaTF.getText(), mesTF.getText(),
								anioTF.getText(), genero);
						try {
							persona.aniadirObjeto(pokeball, 10); // Añadimos 10 pokeballs
							cantidadPokeballsMini.setText(Integer.toString(persona.getCantidadPokeballs()));
							cantidadCoinsMini.setText(Integer.toString(persona.getPokecoins()));
						} catch (BolsilloObjetoLlenoExcepcion a) {
							JOptionPane.showMessageDialog(frmPokemonLion, "El bolsillo de los objetos está lleno.");
						}
						formularioDatosJugador.setVisible(false);
						mensajeBienvenidaJL.setVisible(true);
						continuarBoton.setVisible(true);
						mensajeBienvenidaJL.setText("Encantado de conocerte " + persona.getNombre() + "!");
					}
				}
			}
		});

		comprarBoton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int cantidadPokeballComprar = cantidadPokeballsComprar.getSelectedIndex() + 1;
				int precio = cantidadPokeballComprar * 100;
				funciones.Tienda.comprar(cantidadPokeballComprar, precio, persona, pokeball, frmPokemonLion,
						cantidadCoinsMini, cantidadPokeballsMini, avisoCompraRealizada);
			}
		});

		siguienteBoton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				funciones.OcultarMostrarPaneles.ocultarMostrarPanel(capturadoPanel);
				guardarBoton.setVisible(true);
				especieJL.setText("Especie: ");
				nicelJL.setText("Nivel: ");
				capturadoTitulo.setText(especie.toUpperCase() + " CAPTURADO!");
				especieJL.setText(especieJL.getText() + especie);
				nicelJL.setText(nicelJL.getText() + Integer.toString(nivel));
			}
		});

		capturarPokemonBoton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				capturadoPanel.setVisible(false);
				capturarPokemonBoton.setText("Capturar");
				persona.eliminarObjeto("Pokeball");
				cantidadPokeballsMini.setText(Integer.toString(persona.getCantidadPokeballs()));
				posibilidadCaptura = (float) (Math.random() * 1);
				if (persona.getCantidadPokeballs() == 0) {
					reintentarJL.setText("");
					capturaPanel.setVisible(false);
					pokemonAparecidoJL.setText("");
					JOptionPane.showMessageDialog(frmPokemonLion, "No te quedan Pokeballs.");
					funciones.OcultarMostrarPaneles.ocultarMostrarPanel(menuPrincipalPanel);
				} else {
					funciones.CapturarPokemon.capturando(posibilidadCaptura, frmPokemonLion, escapadoJL, reintentarJL,
							capturadoJL, persona, recompensaJL, informacionPersonalBoton, listarPokemonBoton,
							busquedaBoton, tiendaBoton, cantidadCoinsMini, guardadoJL, ingresarMoteJF,
							fotoPokemonCapturado, guardarBoton, capturarPokemonBoton, especie, siguienteBoton,
							pokemonAparecidoJL);
				}
			}
		});

		guardarBoton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String alias = ingresarMoteJF.getText();
				funciones.CapturarPokemon.guardar(alias, nivel, especie, guardadoJL, guardarBoton, persona,
						informacionPersonalBoton, listarPokemonBoton, busquedaBoton, tiendaBoton, frmPokemonLion);
			}
		});

		salirBoton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				funciones.Salir.despedida(frmPokemonLion);
			}
		});

		verDetallesBoton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int pokemonElegido = listadoPokemonBOX.getSelectedIndex();
				String especieConsultar = persona.getPokemon(pokemonElegido).getClass().getSimpleName();
				funciones.OcultarMostrarPaneles.ocultarMostrarPanel(detallesPokemonPanel);
				funciones.MostrarPokemon.mostrarUnPokemon(pokemonElegido, especieConsultar, nivelEspecieJL,
						tipoEspecieJL, psEspecieJL, ataqueEspecieJL, defensaEspecieJL, capturadoEspecieJL,
						nombrePokemonElegidoJL, persona, fotoPokemonInformacion, ataque1JL, ataque2JL, ataque3JL,
						ataque4JL);
			}
		});

		liberarBoton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				funciones.OcultarMostrarPaneles.ocultarMostrarPanel(pokemonLiberadoPanel);
				pokemonLiberadoJL.setText(
						"Has liberado a " + persona.getPokemon(listadoPokemonBOX.getSelectedIndex()).getNombre());
				persona.liberarPokemon(listadoPokemonBOX.getSelectedIndex());
			}
		});

		detallesAliadoBoton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				enemigo = new Enemigo(especie, nivel);
				pokemonElegido = listaPokemonLuchar.getSelectedIndex();
				String especieConsultar = persona.getPokemon(pokemonElegido).getClass().getSimpleName();
				funciones.Combate.mostrarDetallesAliado(imgPokemonLuchar, especieConsultar, nivelPKElegidoLucha,
						tipoPKElegidoLucha, psPKElegidoLucha, ataquePKElegidoLucha, defensaPKElegidoLucha,
						ataque1PKElegidoLucha, ataque2PKElegidoLucha, ataque3PKElegidoLucha, ataque4PKElegidoLucha,
						persona, pokemonElegido, botonLuchar);
			}
		});

		botonLuchar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelSeleccionPokemon.setVisible(false);
				panelLucha.setVisible(true);

				ataque1Boton.setEnabled(true);
				ataque2Boton.setEnabled(true);
				ataque3Boton.setEnabled(true);
				ataque4Boton.setEnabled(true);
				danioGenerado.setText("");
				ataqueUsado.setText("");

				/*
				 * AudioInputStream audioInputStream = AudioSystem .getAudioInputStream(new
				 * File("src/audios/victoriaCombate.wav").getAbsoluteFile()); Clip clip =
				 * AudioSystem.getClip(); clip.open(audioInputStream);
				 */

				/* Mostramos la imagen del pokemon aliado */
				ImageIcon imagenPokemonAliado = new ImageIcon(new ImageIcon(Main.class.getResource(
						"/imagenesPokemon/" + persona.getPokemon(pokemonElegido).getClass().getSimpleName() + ".gif"))
								.getImage().getScaledInstance(150, 150, Image.SCALE_DEFAULT));
				imgPokemonAmigo.setIcon(imagenPokemonAliado);

				/* Mostramos la imagen del pokemon enemigo */
				ImageIcon imagenPokemonEnemigo = new ImageIcon(
						new ImageIcon(Main.class.getResource("/imagenesPokemon/" + enemigo.getNombre() + ".gif"))
								.getImage().getScaledInstance(150, 150, Image.SCALE_DEFAULT));
				imgPokemonEnemigo.setIcon(imagenPokemonEnemigo);

				/* Generamos los ataques */
				ataque1Boton.setText(persona.getPokemon(pokemonElegido).getAtaques()[0].getNombre() + " | "
						+ persona.getPokemon(pokemonElegido).getAtaques()[0].getPp() + "PP");

				ataque2Boton.setText(persona.getPokemon(pokemonElegido).getAtaques()[1].getNombre() + " | "
						+ persona.getPokemon(pokemonElegido).getAtaques()[1].getPp() + "PP");

				ataque3Boton.setText(persona.getPokemon(pokemonElegido).getAtaques()[2].getNombre() + " | "
						+ persona.getPokemon(pokemonElegido).getAtaques()[2].getPp() + "PP");

				ataque4Boton.setText(persona.getPokemon(pokemonElegido).getAtaques()[3].getNombre() + " | "
						+ persona.getPokemon(pokemonElegido).getAtaques()[3].getPp() + "PP");

				/* Establecemos la vida y el nivel del aliado */
				vidaPokemonAliado.setText("PS: " + persona.getPokemon(pokemonElegido).getPs() + "/"
						+ persona.getPokemon(pokemonElegido).getPsOriginal());
				nivelPokemonAliado.setText("Nivel: " + persona.getPokemon(pokemonElegido).getNivel());

				/* Establecemos la vida y el nivel del enemigo */
				vidaPokemonEnemigo.setText("PS: " + enemigo.getPs() + "/" + enemigo.getPsOriginal());
				nivelPokemonEnemigo.setText("Nivel: " + enemigo.getNivel());
			}
		});

		ataque1Boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				funciones.Combate.boton1(persona, pokemonElegido, ataque1Boton, enemigo, ataqueUsado, danioGenerado,
						vidaPokemonEnemigo, frmPokemonLion, panelLucha, panelCombate, menuPrincipal, ataque2Boton,
						ataque3Boton, ataque4Boton, vidaPokemonAliado, menuPrincipalPanel);
			}
		});

		ataque2Boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				funciones.Combate.boton2(persona, pokemonElegido, ataque1Boton, enemigo, ataqueUsado, danioGenerado,
						vidaPokemonEnemigo, frmPokemonLion, panelLucha, panelCombate, menuPrincipal, ataque2Boton,
						ataque3Boton, ataque4Boton, vidaPokemonAliado, menuPrincipalPanel);
			}
		});

		ataque3Boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				funciones.Combate.boton3(persona, pokemonElegido, ataque1Boton, enemigo, ataqueUsado, danioGenerado,
						vidaPokemonEnemigo, frmPokemonLion, panelLucha, panelCombate, menuPrincipal, ataque2Boton,
						ataque3Boton, ataque4Boton, vidaPokemonAliado, menuPrincipalPanel);
			}
		});

		ataque4Boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				funciones.Combate.boton4(persona, pokemonElegido, ataque1Boton, enemigo, ataqueUsado, danioGenerado,
						vidaPokemonEnemigo, frmPokemonLion, panelLucha, panelCombate, menuPrincipal, ataque2Boton,
						ataque3Boton, ataque4Boton, vidaPokemonAliado, menuPrincipalPanel);
			}
		});

		/* FIN BOTONES ATAQUE */

		botonHuir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				panelCombate.setVisible(false);
				busquedaPanel.setVisible(false);
				panelLucha.setVisible(false);
				menuPrincipal.setVisible(true);
				funciones.OcultarMostrarPaneles.ocultarMostrarPanel(menuPrincipalPanel);
			}
		});

		informacionPersonalBoton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				funciones.OcultarMostrarPaneles.ocultarMostrarPanel(informacionPanel);
				funciones.InformacionJugador.mostrar(mostrarNombreJL, mostrarNacimientoJL, mostrarGeneroJL,
						mostrarPokecoinsJL, mostrarCantidadObjetosJL, mostrarCantidadPokeballsJL,
						mostrarCantidadPokemonJL, persona, iInformacinDelJugadorTitulo, fotoJugador);
			}
		});

		tiendaBoton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				funciones.OcultarMostrarPaneles.ocultarMostrarPanel(tiendaPanel);
				avisoCompraRealizada.setText("");
			}
		});

		busquedaBoton.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				botonLuchar.setEnabled(false);
				if (e.getKeyCode() == 87) {
					personaje.setIcon(new ImageIcon(Main.class.getResource("/imagenesPersonaje/espalda.png")));
					if (y >= 0 && y <= 352 && x >= 0 && x <= 720 && busquedaPanel.isVisible()) {
						y -= 8;
						personaje.setBounds(x, y, 47, 94);
						pasosDados++;
						funciones.Busqueda.opciones(pasosDados, pasosRandom, frmPokemonLion, especie, nivel, persona,
								especieRandom, mostrarPokemonsPanel, capturadoPanel, informacionPanel,
								detallesPokemonPanel, busquedaPanel, tiendaPanel, pokemonLiberadoPanel,
								capturarPokemonBoton, escapadoJL, reintentarJL, capturadoJL, recompensaJL,
								busquedaBoton, pokemonAparecidoJL, fotoPokemonAparecido, menuPrincipal,
								seleccionaUnPokemonJL, listaPokemonLuchar, nivelPKElegidoLucha, tipoPKElegidoLucha,
								psPKElegidoLucha, ataquePKElegidoLucha, defensaPKElegidoLucha, ataque1PKElegidoLucha,
								ataque2PKElegidoLucha, ataque3PKElegidoLucha, ataque4PKElegidoLucha, imgPokemonLuchar,
								botonLuchar, capturaPanel, panelCombate, panelSeleccionPokemon);
						if (pasosDados > pasosRandom) {
							pasosDados = 0;
							especie = funciones.GenerarNivelEspecie.generarEspecie();
							nivel = funciones.GenerarNivelEspecie.generarNivel();
							pasosRandom = (int) (Math.random() * (75 - 5) + 5);
						}
					} else if (y < 5) {
						y = 7;
					}
				}

				if (e.getKeyCode() == 83) {
					personaje.setIcon(new ImageIcon(Main.class.getResource("/imagenesPersonaje/cara.png")));
					if (y >= 0 && y <= 352 && x >= 0 && x <= 720 && busquedaPanel.isVisible()) {
						y += 8;
						personaje.setBounds(x, y, 47, 94);
						pasosDados++;
						funciones.Busqueda.opciones(pasosDados, pasosRandom, frmPokemonLion, especie, nivel, persona,
								especieRandom, mostrarPokemonsPanel, capturadoPanel, informacionPanel,
								detallesPokemonPanel, busquedaPanel, tiendaPanel, pokemonLiberadoPanel,
								capturarPokemonBoton, escapadoJL, reintentarJL, capturadoJL, recompensaJL,
								busquedaBoton, pokemonAparecidoJL, fotoPokemonAparecido, menuPrincipal,
								seleccionaUnPokemonJL, listaPokemonLuchar, nivelPKElegidoLucha, tipoPKElegidoLucha,
								psPKElegidoLucha, ataquePKElegidoLucha, defensaPKElegidoLucha, ataque1PKElegidoLucha,
								ataque2PKElegidoLucha, ataque3PKElegidoLucha, ataque4PKElegidoLucha, imgPokemonLuchar,
								botonLuchar, capturaPanel, panelCombate, panelSeleccionPokemon);
						if (pasosDados > pasosRandom) {
							pasosDados = 0;
							especie = funciones.GenerarNivelEspecie.generarEspecie();
							nivel = funciones.GenerarNivelEspecie.generarNivel();
							pasosRandom = (int) (Math.random() * (75 - 5) + 5);
						}
					} else if (y > 352) {
						y = 347;
					}
				}

				if (e.getKeyCode() == 65) {
					personaje.setIcon(new ImageIcon(Main.class.getResource("/imagenesPersonaje/mirandoIzquierda.png")));
					if (y >= 0 && y <= 352 && x >= 0 && x <= 720 && busquedaPanel.isVisible()) {
						x -= 8;
						personaje.setBounds(x, y, 47, 94);
						pasosDados++;
						funciones.Busqueda.opciones(pasosDados, pasosRandom, frmPokemonLion, especie, nivel, persona,
								especieRandom, mostrarPokemonsPanel, capturadoPanel, informacionPanel,
								detallesPokemonPanel, busquedaPanel, tiendaPanel, pokemonLiberadoPanel,
								capturarPokemonBoton, escapadoJL, reintentarJL, capturadoJL, recompensaJL,
								busquedaBoton, pokemonAparecidoJL, fotoPokemonAparecido, menuPrincipal,
								seleccionaUnPokemonJL, listaPokemonLuchar, nivelPKElegidoLucha, tipoPKElegidoLucha,
								psPKElegidoLucha, ataquePKElegidoLucha, defensaPKElegidoLucha, ataque1PKElegidoLucha,
								ataque2PKElegidoLucha, ataque3PKElegidoLucha, ataque4PKElegidoLucha, imgPokemonLuchar,
								botonLuchar, capturaPanel, panelCombate, panelSeleccionPokemon);
						if (pasosDados > pasosRandom) {
							pasosDados = 0;
							especie = funciones.GenerarNivelEspecie.generarEspecie();
							nivel = funciones.GenerarNivelEspecie.generarNivel();
							pasosRandom = (int) (Math.random() * (75 - 5) + 5);
						}
					} else if (x < 0) {
						x = 4;
					}
				}

				if (e.getKeyCode() == 68) {
					personaje.setIcon(new ImageIcon(Main.class.getResource("/imagenesPersonaje/mirandoDerecha.png")));
					if (y >= 0 && y <= 352 && x >= 0 && x <= 720 && busquedaPanel.isVisible()) {
						x += 8;
						personaje.setBounds(x, y, 47, 94);
						pasosDados++;
						funciones.Busqueda.opciones(pasosDados, pasosRandom, frmPokemonLion, especie, nivel, persona,
								especieRandom, mostrarPokemonsPanel, capturadoPanel, informacionPanel,
								detallesPokemonPanel, busquedaPanel, tiendaPanel, pokemonLiberadoPanel,
								capturarPokemonBoton, escapadoJL, reintentarJL, capturadoJL, recompensaJL,
								busquedaBoton, pokemonAparecidoJL, fotoPokemonAparecido, menuPrincipal,
								seleccionaUnPokemonJL, listaPokemonLuchar, nivelPKElegidoLucha, tipoPKElegidoLucha,
								psPKElegidoLucha, ataquePKElegidoLucha, defensaPKElegidoLucha, ataque1PKElegidoLucha,
								ataque2PKElegidoLucha, ataque3PKElegidoLucha, ataque4PKElegidoLucha, imgPokemonLuchar,
								botonLuchar, capturaPanel, panelCombate, panelSeleccionPokemon);
						if (pasosDados > pasosRandom) {
							pasosDados = 0;
							especie = funciones.GenerarNivelEspecie.generarEspecie();
							nivel = funciones.GenerarNivelEspecie.generarNivel();
							pasosRandom = (int) (Math.random() * (75 - 5) + 5);
						}
					} else if (x > 720) {
						x = 718;
					}
				}
			}
		});

		listarPokemonBoton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				funciones.OcultarMostrarPaneles.ocultarMostrarPanel(mostrarPokemonsPanel);
				funciones.MostrarPokemon.listarPokemon(persona, frmPokemonLion, listadoPokemonBOX);
			}
		});

		busquedaBoton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pasosDados = 0;
				personaje.setIcon(new ImageIcon(Main.class.getResource("/imagenesPersonaje/personaje.png")));
				especie = funciones.GenerarNivelEspecie.generarEspecie();
				nivel = funciones.GenerarNivelEspecie.generarNivel();
				funciones.OcultarMostrarPaneles.ocultarMostrarPanel(busquedaPanel);
				pasosRandom = (int) (Math.random() * (75 - 5) + 5);
			}
		});
	}
}
