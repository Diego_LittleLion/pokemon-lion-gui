package lion;

public class BolsilloPokemonLlenoExcepcion extends Exception{
	private int cantidad;
	public BolsilloPokemonLlenoExcepcion (int cantidad) {
		this.cantidad = cantidad;
	}
	
	@Override
    public String toString () {
        return "Bolsillo lleno! No puedes guardar más Pokemons." 
        		+ "\n" + "El tamaño máximo del bolsillo de pokemons es de "
        		+ cantidad + ". \n";
    }
}
