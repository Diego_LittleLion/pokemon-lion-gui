package lion;

import pokemon.Pokemon;

public class Mochila {
	private Pokemon[] bolsilloPokemon;
	private Objetos[] bolsilloObjetos;
	private int cantidadPokemons;
	private int cantidadObjetos;

	public Mochila() {
		bolsilloPokemon = new Pokemon[51];
		bolsilloObjetos = new Objetos[51];
		cantidadPokemons = 0;
		cantidadObjetos = 0;
	}

	/* Métodos para trabajar con el bolsillo de los Pokemon */
	public Pokemon[] getBolsilloPokemon() {
		return bolsilloPokemon;
	}
	
	public Pokemon getPokemon(int posicion) {
		return bolsilloPokemon[posicion];
	}

	public void liberarPokemon(int posicion) {
		for (int i = posicion; i <= cantidadPokemons; i++) {
			bolsilloPokemon[posicion] = bolsilloPokemon[posicion + 1];
		}
		cantidadPokemons--;
	}

	public void aniadirPokemon(Pokemon nuevo) throws BolsilloPokemonLlenoExcepcion {
		try {
			bolsilloPokemon[cantidadPokemons] = nuevo;
			cantidadPokemons++;
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new BolsilloPokemonLlenoExcepcion(cantidadPokemons);
		}
	}

	public int getCantidadPokemons() {
		return cantidadPokemons;
	}

	/* Métodos para trabajar con el bolsillo de los objetos */
	
	public int getCantidadPokeballs() {
		int cantidad = 0;
		for(int i = 0; i < cantidadObjetos; i++) {
			if(bolsilloObjetos[i].getNombre().equals("Pokeball")) {
				cantidad++;
			}
		}
		return cantidad;
	}
	
	public void aniadirObjeto(Objetos objeto, int cantidad) throws BolsilloObjetoLlenoExcepcion {
		try {
			for (int i = 0; i < cantidad; i++) {
				bolsilloObjetos[cantidadObjetos] = objeto;
				cantidadObjetos++;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new BolsilloObjetoLlenoExcepcion(cantidadObjetos);
		}
	}
	
	public void eliminarObjeto(String objeto) {
		int posicionPaBorrar = 0;
		for(int i = 0; i < cantidadObjetos; i++){
			if(bolsilloObjetos[i].getNombre().equals(objeto)) {
				posicionPaBorrar = i;
			}
		}
		for (int i = posicionPaBorrar; i <= cantidadObjetos; i++) {
			bolsilloObjetos[posicionPaBorrar] = bolsilloObjetos[posicionPaBorrar + 1];
		}
		cantidadObjetos--;
	}
	
	public Objetos getObjetos(int i) {
		return bolsilloObjetos[i];
	}

	public int getCantidadObjetos() {
		return cantidadObjetos;
	}

}
