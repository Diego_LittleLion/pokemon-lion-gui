package lion;

import pokemon.Pokemon;

public class Personaje {
	private String nombre;
	private String dia;
	private String mes;
	private String anio;
	private String genero_personaje;
	private String fecha_nacimiento;
	private int pokecoins;
	private Mochila mochila;

	public Personaje(String nombre, String dia, String mes, String anio, String genero_personaje) {
		this.nombre = nombre;
		this.fecha_nacimiento = dia + "/" + mes + "/" + anio;
		this.dia = dia;
		this.mes = mes;
		this.anio = anio;
		this.genero_personaje = genero_personaje;
		this.pokecoins = 100;
		this.mochila = new Mochila();
	}

	/* Métodos para trabajar con los pokemons */
	public void aniadirPokemon(Pokemon nuevo) throws BolsilloPokemonLlenoExcepcion {
		try {
			this.mochila.aniadirPokemon(nuevo);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new BolsilloPokemonLlenoExcepcion(this.mochila.getCantidadPokemons());
		}
	}

	public void liberarPokemon(int posicion) {
		this.mochila.liberarPokemon(posicion);
	}

	public int getCantidadPokemons() {
		return this.mochila.getCantidadPokemons();
	}

	public Pokemon getPokemon(int posicion) {
		return this.mochila.getPokemon(posicion);
	}
	
	public Pokemon[] getBolsilloPokemon() {
		return this.mochila.getBolsilloPokemon();
	}

	/* Métodos para trabajar con los objetos */
	public int getCantidadPokeballs() {
		return this.mochila.getCantidadPokeballs();
	}
	
	public void aniadirObjeto(Objetos objeto, int cantidad) throws BolsilloObjetoLlenoExcepcion{
		try {
			this.mochila.aniadirObjeto(objeto, cantidad);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new BolsilloObjetoLlenoExcepcion(this.mochila.getCantidadObjetos());
		}
	}
	
	public void eliminarObjeto(String objeto) {
		this.mochila.eliminarObjeto(objeto);		
	}
	
	public int getCantidadObjetos() {
		return this.mochila.getCantidadObjetos();
	}
	
	public Objetos getObjetos(int i) {
		return this.mochila.getObjetos(i);
	}
	
	/* Métodos para trabajar con el jugador */
	public int getPokecoins() {
		return pokecoins;
	}

	public void setPokecoins(int pokecoins) {
		this.pokecoins = pokecoins;
	}

	public String getNombre() {
		return nombre;
	}

	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public String getGeneroPersonaje() {
		return genero_personaje;
	}

}
