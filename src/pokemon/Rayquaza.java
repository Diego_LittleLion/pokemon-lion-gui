package pokemon;

import ataques.*;
/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Rayquaza extends Pokemon {
	private PulsoDragon pulso = new PulsoDragon();
	private TajoAereo tajo = new TajoAereo();
	private Triturar triturar = new Triturar();
	private Hiperrayo hiperrayo = new Hiperrayo();
	public Rayquaza(String nombre, int nivel) {
		this.tipo = new String[2];
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo[0] = "Volador";
		this.tipo[1] = "Dragon";
		this.ps = nivel * 99;
		this.psOriginal = nivel * 99;
		this.ataque = nivel * 99;
		this.defensa = 99;
		this.ataques[0] = pulso;
		this.ataques[1] = tajo;
		this.ataques[2] = triturar;
		this.ataques[3] = hiperrayo;
	}
}
