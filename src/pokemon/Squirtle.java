package pokemon;

import ataques.*;
/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Squirtle extends Pokemon {
	private Placaje placaje = new Placaje();
	private Burbuja burbuja = new Burbuja();
	private PistolaAgua pistola = new PistolaAgua();
	private Hidrobomba hidrobomba = new Hidrobomba();
	public Squirtle(String nombre, int nivel) {
		this.tipo = new String[1];
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo[0] = "Agua";
		this.ps = nivel * 13;
		this.psOriginal = nivel * 13;
		this.ataque = nivel * 25;
		this.defensa = 2;
		this.ataques[0] = placaje;
		this.ataques[1] = burbuja;
		this.ataques[2] = pistola;
		this.ataques[3] = hidrobomba;
	}
}
