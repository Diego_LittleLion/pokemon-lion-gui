package pokemon;

import ataques.*;
import lion.Pokemontura;
/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Pidgey extends Pokemon implements Pokemontura{
	private Placaje placaje = new Placaje();
	private AtaqueRapido aRapido = new AtaqueRapido();
	private TajoAereo tajo = new TajoAereo();
	private Vendaval vendaval = new Vendaval();
	/**
	 * Genera a Pidgey
	 * @param nombre - nombre del pokemon elegido por el usuario
	 * @param nivel - nivel del pokemon generado aleatoriamente
	 */
	public Pidgey (String nombre, int nivel) {
		this.tipo = new String[2];
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo[0] = "Volador";
		this.tipo[1] = "Normal";
		this.ps = nivel * 15;
		this.psOriginal = nivel * 15;
		this.ataque = nivel * 8;
		this.defensa = 2;
		this.ataques[0] = placaje;
		this.ataques[1] = aRapido;
		this.ataques[2] = tajo;
		this.ataques[3] = vendaval;
	}
	
	@Override
	public String montar() {
		String salida = "Te has montado en " + nombre + " correctamente ;) !";
		return salida;
	}
}
