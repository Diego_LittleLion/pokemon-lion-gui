package pokemon;

import ataques.*;
/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Enemigo extends Pokemon {
	private Lenguetazo lenguetazo = new Lenguetazo();
	private Pisoton pisoton = new Pisoton();
	private Latigazo latigazo = new Latigazo();
	private Portazo portazo = new Portazo();
	private Placaje placaje = new Placaje();
	private Burbuja burbuja = new Burbuja();
	private PistolaAgua pistola = new PistolaAgua();
	private Hidrobomba hidrobomba = new Hidrobomba();
	private PulsoDragon pulso = new PulsoDragon();
	private TajoAereo tajo = new TajoAereo();
	private Triturar triturar = new Triturar();
	private Hiperrayo hiperrayo = new Hiperrayo();
	private Impactrueno impactrueno = new Impactrueno();
	private AtaqueRapido aRapido = new AtaqueRapido();
	private Trueno trueno = new Trueno();
	private Chispa chispa = new Chispa();
	private Vendaval vendaval = new Vendaval();
	private DisparoDemora disparo = new DisparoDemora();
	private NoAtaque noataque = new NoAtaque();
	private Picadura picadura = new Picadura();

	/**
	 * Genera al enemigo
	 * @param especie - especie del pokemon generado aleatoriamente
	 * @param nivel - nivel del pokemon generado aleatoriamente
	 */
	public Enemigo(String especie, int nivel) {
		this.nombre = especie;
		this.nivel = nivel;
		switch (especie) {
		case "Caterpie":
			this.tipo = new String[1];
			this.ps = nivel * 1;
			this.psOriginal = nivel * 1;
			this.ataque = nivel * 1;
			this.defensa = 1;
			this.tipo[0] = "Bicho";
			this.ataques[0] = placaje;
			this.ataques[1] = disparo;
			this.ataques[2] = picadura;
			this.ataques[3] = noataque;
			break;
		case "Lickitung":
			this.tipo = new String[1];
			this.tipo[0] = "Normal";
			this.ps = nivel * 12;
			this.psOriginal = nivel * 12;
			this.ataque = nivel * 7;
			this.defensa = 3;
			this.ataques[0] = lenguetazo;
			this.ataques[1] = pisoton;
			this.ataques[2] = latigazo;
			this.ataques[3] = portazo;
			break;
		case "Pidgey":
			this.tipo = new String[2];
			this.tipo[0] = "Volador";
			this.tipo[1] = "Normal";
			this.ps = nivel * 15;
			this.psOriginal = nivel * 15;
			this.ataque = nivel * 8;
			this.defensa = 2;
			this.ataques[0] = placaje;
			this.ataques[1] = aRapido;
			this.ataques[2] = tajo;
			this.ataques[3] = vendaval;
			break;
		case "Pikachu":
			this.tipo = new String[1];
			this.tipo[0] = "Electrico";
			this.ps = nivel * 20;
			this.psOriginal = nivel * 20;
			this.ataque = nivel * 6;
			this.defensa = 2;
			this.ataques[0] = impactrueno;
			this.ataques[1] = aRapido;
			this.ataques[2] = trueno;
			this.ataques[3] = chispa;
			break;
		case "Squirtle":
			this.tipo = new String[1];
			this.ps = nivel * 13;
			this.psOriginal = nivel * 13;
			this.ataque = nivel * 25;
			this.defensa = 2;
			this.ataques[0] = placaje;
			this.ataques[1] = burbuja;
			this.ataques[2] = pistola;
			this.ataques[3] = hidrobomba;
			break;
		case "Rayquaza":
			this.tipo = new String[2];
			this.ps = nivel * 99;
			this.psOriginal = nivel * 99;
			this.ataque = nivel * 99;
			this.defensa = 99;
			this.ataques[0] = pulso;
			this.ataques[1] = tajo;
			this.ataques[2] = triturar;
			this.ataques[3] = hiperrayo;
			break;
		}
	}
}
