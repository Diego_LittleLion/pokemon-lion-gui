package pokemon;

import ataques.*;

/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Lickitung extends Pokemon{
	private Lenguetazo lenguetazo = new Lenguetazo();
	private Pisoton pisoton = new Pisoton();
	private Latigazo latigazo = new Latigazo();
	private Portazo portazo = new Portazo();
	/**
	 * Genera a Lickitung
	 * @param nombre - nombre del pokemon elegido por el usuario
	 * @param nivel - nivel del pokemon generado aleatoriamente
	 */
	public Lickitung(String nombre, int nivel) {
		this.tipo = new String[1];
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo[0] = "Normal";
		this.ps = nivel * 12;
		this.psOriginal = nivel * 12;
		this.ataque = nivel * 7;
		this.defensa = 3;
		this.ataques[0] = lenguetazo;
		this.ataques[1] = pisoton;
		this.ataques[2] = latigazo;
		this.ataques[3] = portazo;
	}
}
