package pokemon;

import ataques.*;
/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Pikachu extends Pokemon {
	private Impactrueno impactrueno = new Impactrueno();
	private AtaqueRapido aRapido = new AtaqueRapido();
	private Trueno trueno = new Trueno();
	private Chispa chispa = new Chispa();
	/**
	 * Genera a Pikachu
	 * @param nombre - nombre del pokemon elegido por el usuario
	 * @param nivel - nivel del pokemon generado aleatoriamente
	 */
	public Pikachu(String nombre, int nivel) {
		this.tipo = new String[1];
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo[0] = "Electrico";
		this.ps = nivel * 20;
		this.psOriginal = nivel * 20;
		this.ataque = nivel * 6;
		this.defensa = 2;
		this.ataques[0] = impactrueno;
		this.ataques[1] = aRapido;
		this.ataques[2] = trueno;
		this.ataques[3] = chispa;
	}
}