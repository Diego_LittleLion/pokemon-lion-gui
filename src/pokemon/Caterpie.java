package pokemon;

import ataques.*;
import lion.Pokemontura;

/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Caterpie extends Pokemon implements Pokemontura{
	private Placaje placaje = new Placaje();
	private DisparoDemora disparo = new DisparoDemora();
	private NoAtaque noataque = new NoAtaque();
	private Picadura picadura = new Picadura();
	/**
	 * Genera a Caterpie
	 * @param nombre - nombre del pokemon elegido por el usuario
	 * @param nivel - nivel del pokemon generado aleatoriamente
	 */
	public Caterpie(String nombre, int nivel) {
		this.tipo = new String[1];
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo[0] = "Bicho";
		this.ps = nivel * 1;
		this.psOriginal = nivel * 1;
		this.ataque = nivel * 1;
		this.defensa = 1;
		this.ataques[0] = placaje;
		this.ataques[1] = disparo;
		this.ataques[2] = picadura;
		this.ataques[3] = noataque;
	}

	@Override
	public String montar() {
		String salida = "Te has montado en " + nombre + " correctamente ;) !";
		return salida;
	}
}
