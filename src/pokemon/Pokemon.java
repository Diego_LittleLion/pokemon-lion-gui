package pokemon;

import java.sql.Timestamp;
import java.util.Date;

import ataques.Ataque;
/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public abstract class Pokemon {
	protected String nombre;
	protected int nivel;
	protected String[] tipo;
	protected Ataque[] ataques = new Ataque[4];
	protected int ps;
	protected int psOriginal;
	protected int ataque;
	protected int defensa;
	protected Timestamp momentoCaptura = new Timestamp(new Date().getTime());

	/**
	 * Devuelve el nombre del pokemon
	 * @return devuelve la variable nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Devuelve el nivel del pokemon
	 * @return devuelve la variable nivel
	 */
	public int getNivel() {
		return nivel;
	}

	/**
	 * Devuelve la lista de tipos del pokemon
	 * @return devuelve el array tipo
	 */
	public String[] getTipo() {
		return tipo;
	}
	
	/**
	 * Devuelve la lista de ataques del pokemon
	 * @return devuelve el array ataques
	 */
	public Ataque[] getAtaques() {
		return ataques;
	}

	/**
	 * Devuelve la vida del pokemon
	 * @return devuelve la variable ps
	 */
	public int getPs() {
		return ps;
	}
	
	/**
	 * Modifica la variable ps
	 * @param ps
	 */
	public void setPs(int ps) {
		this.ps = ps;
	}
	
	/**
	 * Devuelve la vida original del pokemon
	 * @return devuelve la variable psOriginal
	 */
	public int getPsOriginal() {
		return psOriginal;
	}

	/**
	 * Devuelve el ataque del pokemon
	 * @return devuelve la variable ataque
	 */
	public int getAtaque() {
		return ataque;
	}
	
	/**
	 * Devuelve la defensa del pokemon
	 * @return devuelve la variable defensa
	 */
	public int getDefensa() {
		return defensa;
	}
	
	/**
	 * Devuelve el momento de captura del pokemon
	 * @return devuelve la variable momentoCaptura
	 */
	public Timestamp getMomentoCaptura() {
		return momentoCaptura;
	}

}
