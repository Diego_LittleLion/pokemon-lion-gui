package funciones;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Fecha {

	/**
	 * 
	 * @param dia - dia de nacimiento que ha introducido el jugador
	 * @param mes - mes de nacimiento que ha introducido el jugador
	 * @param anio - año de nacimiento que ha introducido el jugador
	 * @param frmPokemonLion - le pasamos el frame por si tenemos que utilizar un cartel emergente
	 * @return devuelve true o false dependiendo de si la fecha es erronea o no
	 */
	public static boolean comprobarfecha(String dia, String mes, String anio, JFrame frmPokemonLion) {
		int posicion;
		
		try {	
			int datodia = Integer.parseInt(dia);
			int datomes = Integer.parseInt(mes);
			int datoanio = Integer.parseInt(anio);
			
			if(datomes == 2 && datodia > 28) {
				JOptionPane.showMessageDialog(frmPokemonLion, "La fecha es errona, prueba otra vez (Febrero solo tiene 28 días)");
				return true;
			}
	
			if (datodia < 1 || datodia > 31 || datomes < 1 || datomes > 12 || datoanio < 1950 || datoanio > 2019) {
				JOptionPane.showMessageDialog(frmPokemonLion, "La fecha es errona, prueba otra vez");
				return true;
			} else {
				return false;
			}
		}catch(StringIndexOutOfBoundsException e) {
			return true;
		}
	}
}
