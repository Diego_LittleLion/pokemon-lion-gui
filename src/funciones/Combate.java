package funciones;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import lion.Main;
import lion.Personaje;
import pokemon.*;

public class Combate {

	public static void elegirPokemon(JLabel seleccionaUnPokemonJL, String especie, JComboBox listaPokemonLuchar,
			Personaje persona, int nivel, JLabel nivelPKElegidoLucha, JLabel tipoPKElegidoLucha,
			JLabel psPKElegidoLucha, JLabel ataquePKElegidoLucha, JLabel defensaPKElegidoLucha,
			JLabel ataque1pkElegidoLucha, JLabel ataque2pkElegidoLucha, JLabel ataque3pkElegidoLucha,
			JLabel ataque4pkElegidoLucha, JLabel imgPokemonLuchar, JButton botonLuchar) {
		imgPokemonLuchar.setVisible(false);
		nivelPKElegidoLucha.setText("Nivel: ");
		tipoPKElegidoLucha.setText("Tipo: ");
		psPKElegidoLucha.setText("PS: ");
		ataquePKElegidoLucha.setText("Ataque: ");
		defensaPKElegidoLucha.setText("Defensa: ");
		ataque1pkElegidoLucha.setText("Ataque 1:  ");
		ataque2pkElegidoLucha.setText("Ataque 2:  ");
		ataque3pkElegidoLucha.setText("Ataque 3:  ");
		ataque4pkElegidoLucha.setText("Ataque 4:  ");
		seleccionaUnPokemonJL.setText("Selecciona un Pokemon para combatir contra " + especie);
		listaPokemonLuchar.removeAllItems();
		for (int i = 0; i < persona.getCantidadPokemons(); i++) {
			listaPokemonLuchar.addItem((i + 1) + ".-  " + persona.getPokemon(i).getNombre() + " ("
					+ persona.getPokemon(i).getClass().getSimpleName() + ") - lvl " + persona.getPokemon(i).getNivel());
		}
	}

	public static void mostrarDetallesAliado(JLabel imgPokemonLuchar, String especie, JLabel nivelPKElegidoLucha,
			JLabel tipoPKElegidoLucha, JLabel psPKElegidoLucha, JLabel ataquePKElegidoLucha,
			JLabel defensaPKElegidoLucha, JLabel ataque1pkElegidoLucha, JLabel ataque2pkElegidoLucha,
			JLabel ataque3pkElegidoLucha, JLabel ataque4pkElegidoLucha, Personaje persona, int pokemonElegido,
			JButton botonLuchar) {
		nivelPKElegidoLucha.setText("Nivel: ");
		tipoPKElegidoLucha.setText("Tipo: ");
		psPKElegidoLucha.setText("PS: ");
		ataquePKElegidoLucha.setText("Ataque: ");
		defensaPKElegidoLucha.setText("Defensa: ");
		ataque1pkElegidoLucha.setText("Ataque 1:  ");
		ataque2pkElegidoLucha.setText("Ataque 2:  ");
		ataque3pkElegidoLucha.setText("Ataque 3:  ");
		ataque4pkElegidoLucha.setText("Ataque 4:  ");
		nivelPKElegidoLucha.setVisible(true);
		tipoPKElegidoLucha.setVisible(true);
		psPKElegidoLucha.setVisible(true);
		ataquePKElegidoLucha.setVisible(true);
		defensaPKElegidoLucha.setVisible(true);
		ataque1pkElegidoLucha.setVisible(true);
		ataque2pkElegidoLucha.setVisible(true);
		ataque3pkElegidoLucha.setVisible(true);
		ataque4pkElegidoLucha.setVisible(true);
		imgPokemonLuchar.setVisible(true);

		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon(Main.class.getResource("/imagenesPokemon/" + especie + ".gif")).getImage()
						.getScaledInstance(150, 150, Image.SCALE_DEFAULT));
		imgPokemonLuchar.setIcon(imageIcon);

		nivelPKElegidoLucha.setText(nivelPKElegidoLucha.getText() + persona.getPokemon(pokemonElegido).getNivel());

		if (persona.getPokemon(pokemonElegido).getTipo().length == 1) {
			tipoPKElegidoLucha.setText(tipoPKElegidoLucha.getText() + persona.getPokemon(pokemonElegido).getTipo()[0]);
		} else {
			tipoPKElegidoLucha.setText(tipoPKElegidoLucha.getText() + persona.getPokemon(pokemonElegido).getTipo()[0]
					+ " | " + persona.getPokemon(pokemonElegido).getTipo()[1]);
		}

		psPKElegidoLucha.setText(psPKElegidoLucha.getText() + persona.getPokemon(pokemonElegido).getPs());
		ataquePKElegidoLucha.setText(ataquePKElegidoLucha.getText() + persona.getPokemon(pokemonElegido).getAtaque());
		defensaPKElegidoLucha
				.setText(defensaPKElegidoLucha.getText() + persona.getPokemon(pokemonElegido).getDefensa());

		ataque1pkElegidoLucha.setText(
				ataque1pkElegidoLucha.getText() + persona.getPokemon(pokemonElegido).getAtaques()[0].getNombre() + " | "
						+ persona.getPokemon(pokemonElegido).getAtaques()[0].getPp() + "PP | "
						+ persona.getPokemon(pokemonElegido).getAtaques()[0].getTipo());

		ataque2pkElegidoLucha.setText(
				ataque2pkElegidoLucha.getText() + persona.getPokemon(pokemonElegido).getAtaques()[1].getNombre() + " | "
						+ persona.getPokemon(pokemonElegido).getAtaques()[1].getPp() + "PP | "
						+ persona.getPokemon(pokemonElegido).getAtaques()[1].getTipo());

		ataque3pkElegidoLucha.setText(
				ataque3pkElegidoLucha.getText() + persona.getPokemon(pokemonElegido).getAtaques()[2].getNombre() + " | "
						+ persona.getPokemon(pokemonElegido).getAtaques()[2].getPp() + "PP | "
						+ persona.getPokemon(pokemonElegido).getAtaques()[2].getTipo());

		ataque4pkElegidoLucha.setText(
				ataque4pkElegidoLucha.getText() + persona.getPokemon(pokemonElegido).getAtaques()[3].getNombre() + " | "
						+ persona.getPokemon(pokemonElegido).getAtaques()[3].getPp() + "PP | "
						+ persona.getPokemon(pokemonElegido).getAtaques()[3].getTipo());
		botonLuchar.setEnabled(true);
	}

	/*
	 * 0.01 * ( (0.2*nivelEnemigo+1) * ataquePokemon * potenciaAtaque / (25 *
	 * defensaAliado) + 2)
	 */
	public static void ataqueEnemigo(JButton ataque1Boton, JButton ataque2Boton, JButton ataque3Boton,
			JButton ataque4Boton, Enemigo enemigo, Personaje persona, JLabel ataqueUsado, JLabel danioGenerado,
			int pokemonAliadoElegido, JLabel vidaPokemonAliado, JFrame frmPokemonLion, JPanel panelLucha,
			JPanel panelCombate, JPanel menuPrincipal, JPanel menuPrincipalPanel) {

		int ataqueRandom = (int) (Math.random() * (4 - 1) + 1);
		int danio = (int) (0.01 * ((0.2 * enemigo.getNivel() + 1) * enemigo.getAtaque()
				* enemigo.getAtaques()[ataqueRandom].getPotencia()
				/ (25 * persona.getPokemon(pokemonAliadoElegido).getDefensa()) + 2));
		ataque1Boton.setEnabled(false);
		ataque2Boton.setEnabled(false);
		ataque3Boton.setEnabled(false);
		ataque4Boton.setEnabled(false);
		JOptionPane.showMessageDialog(frmPokemonLion, "Es el turno del " + enemigo.getNombre() + " enemigo");
		ataqueUsado.setText(enemigo.getNombre() + " ha usado " + enemigo.getAtaques()[ataqueRandom].getNombre());
		danioGenerado
				.setText("Ha hecho " + danio + " de daño a " + persona.getPokemon(pokemonAliadoElegido).getNombre());
		persona.getPokemon(pokemonAliadoElegido).setPs(persona.getPokemon(pokemonAliadoElegido).getPs() - danio);
		vidaPokemonAliado.setText("PS: " + persona.getPokemon(pokemonAliadoElegido).getPs() + "/"
				+ persona.getPokemon(pokemonAliadoElegido).getPsOriginal());
		ataque1Boton.setEnabled(true);
		ataque2Boton.setEnabled(true);
		ataque3Boton.setEnabled(true);
		ataque4Boton.setEnabled(true);
		if (persona.getPokemon(pokemonAliadoElegido).getPs() <= 0) {
			vidaPokemonAliado.setText("PS: 0/" + persona.getPokemon(pokemonAliadoElegido).getPsOriginal());
			persona.getPokemon(pokemonAliadoElegido).setPs(0);
			JOptionPane.showMessageDialog(frmPokemonLion,
					persona.getPokemon(pokemonAliadoElegido).getNombre() + " se debilitó! Has perdido!");
			panelLucha.setVisible(false);
			panelCombate.setVisible(false);
			menuPrincipal.setVisible(true);
			funciones.OcultarMostrarPaneles.ocultarMostrarPanel(menuPrincipalPanel);
		}
	}

	/*
	 * daño = 0.01 * ( (0.2*nivelAliado+1) * ataquePokemon * potenciaAtaque / (25 *
	 * defensaEnemigo) + 2)
	 */
	/* https://pokemon.fandom.com/es/wiki/Da%C3%B1o */
	public static void boton1(Personaje persona, int pokemonElegido, JButton ataque1Boton, Enemigo enemigo,
			JLabel ataqueUsado, JLabel danioGenerado, JLabel vidaPokemonEnemigo, JFrame frmPokemonLion,
			JPanel panelLucha, JPanel panelCombate, JPanel menuPrincipal, JButton ataque2Boton, JButton ataque3Boton,
			JButton ataque4Boton, JLabel vidaPokemonAliado, JPanel menuPrincipalPanel) {
		if (persona.getPokemon(pokemonElegido).getAtaques()[0].getPp() > 0) {
			ataque1Boton.setEnabled(true);
			persona.getPokemon(pokemonElegido).getAtaques()[0]
					.setPp(persona.getPokemon(pokemonElegido).getAtaques()[0].getPp() - 1);
			ataque1Boton.setText(persona.getPokemon(pokemonElegido).getAtaques()[0].getNombre() + " | "
					+ persona.getPokemon(pokemonElegido).getAtaques()[0].getPp() + "PP");

			int danio = (int) (0.01 * ((0.2 * persona.getPokemon(pokemonElegido).getNivel() + 1)
					* persona.getPokemon(pokemonElegido).getAtaque()
					* persona.getPokemon(pokemonElegido).getAtaques()[0].getPotencia() / (25 * enemigo.getDefensa())
					+ 2));

			enemigo.setPs(enemigo.getPs() - danio);
			ataqueUsado.setText(persona.getPokemon(pokemonElegido).getNombre() + " ha usado "
					+ persona.getPokemon(pokemonElegido).getAtaques()[0].getNombre());
			danioGenerado.setText("Ha hecho " + danio + " de daño al " + enemigo.getNombre() + " enemigo.");
			vidaPokemonEnemigo.setText("PS: " + enemigo.getPs() + "/" + enemigo.getPsOriginal());
			/* Hacemos que el enemigo ataque */
			if (enemigo.getPs() <= 0) {
				vidaPokemonEnemigo.setText("PS: 0/" + enemigo.getPsOriginal());
				// clip.start();
				JOptionPane.showMessageDialog(frmPokemonLion, "El " + enemigo.getNombre() + " se ha debilitado!");
				panelLucha.setVisible(false);
				panelCombate.setVisible(false);
				menuPrincipal.setVisible(true);
				funciones.OcultarMostrarPaneles.ocultarMostrarPanel(menuPrincipalPanel);
			} else {
				funciones.Combate.ataqueEnemigo(ataque1Boton, ataque2Boton, ataque3Boton, ataque4Boton, enemigo,
						persona, ataqueUsado, danioGenerado, pokemonElegido, vidaPokemonAliado, frmPokemonLion,
						panelCombate, panelCombate, menuPrincipal, menuPrincipalPanel);
			}
		} else {
			ataque1Boton.setEnabled(false);
		}
	}

	public static void boton2(Personaje persona, int pokemonElegido, JButton ataque1Boton, Enemigo enemigo,
			JLabel ataqueUsado, JLabel danioGenerado, JLabel vidaPokemonEnemigo, JFrame frmPokemonLion,
			JPanel panelLucha, JPanel panelCombate, JPanel menuPrincipal, JButton ataque2Boton, JButton ataque3Boton,
			JButton ataque4Boton, JLabel vidaPokemonAliado, JPanel menuPrincipalPanel) {
		if (persona.getPokemon(pokemonElegido).getAtaques()[1].getPp() > 0) {
			ataque2Boton.setEnabled(true);
			persona.getPokemon(pokemonElegido).getAtaques()[1]
					.setPp(persona.getPokemon(pokemonElegido).getAtaques()[1].getPp() - 1);
			ataque2Boton.setText(persona.getPokemon(pokemonElegido).getAtaques()[1].getNombre() + " | "
					+ persona.getPokemon(pokemonElegido).getAtaques()[1].getPp() + "PP");

			int danio = (int) (0.01 * ((0.2 * persona.getPokemon(pokemonElegido).getNivel() + 1)
					* persona.getPokemon(pokemonElegido).getAtaque()
					* persona.getPokemon(pokemonElegido).getAtaques()[1].getPotencia() / (25 * enemigo.getDefensa())
					+ 2));

			enemigo.setPs(enemigo.getPs() - danio);
			ataqueUsado.setText(persona.getPokemon(pokemonElegido).getNombre() + " ha usado "
					+ persona.getPokemon(pokemonElegido).getAtaques()[1].getNombre());
			danioGenerado.setText("Ha hecho " + danio + " de daño al " + enemigo.getNombre() + " enemigo.");
			vidaPokemonEnemigo.setText("PS: " + enemigo.getPs() + "/" + enemigo.getPsOriginal());
			/* Hacemos que el enemigo ataque */
			if (enemigo.getPs() <= 0) {
				vidaPokemonEnemigo.setText("PS: 0/" + enemigo.getPsOriginal());
				// clip.start();
				JOptionPane.showMessageDialog(frmPokemonLion, "El " + enemigo.getNombre() + " se ha debilitado!");
				panelLucha.setVisible(false);
				panelCombate.setVisible(false);
				menuPrincipal.setVisible(true);
				funciones.OcultarMostrarPaneles.ocultarMostrarPanel(menuPrincipalPanel);
			} else {
				funciones.Combate.ataqueEnemigo(ataque1Boton, ataque2Boton, ataque3Boton, ataque4Boton, enemigo,
						persona, ataqueUsado, danioGenerado, pokemonElegido, vidaPokemonAliado, frmPokemonLion,
						panelCombate, panelCombate, menuPrincipal, menuPrincipalPanel);
			}
		} else {
			ataque2Boton.setEnabled(false);
		}
	}

	public static void boton3(Personaje persona, int pokemonElegido, JButton ataque1Boton, Enemigo enemigo,
			JLabel ataqueUsado, JLabel danioGenerado, JLabel vidaPokemonEnemigo, JFrame frmPokemonLion,
			JPanel panelLucha, JPanel panelCombate, JPanel menuPrincipal, JButton ataque2Boton, JButton ataque3Boton,
			JButton ataque4Boton, JLabel vidaPokemonAliado, JPanel menuPrincipalPanel) {
		if (persona.getPokemon(pokemonElegido).getAtaques()[2].getPp() > 0) {
			ataque3Boton.setEnabled(true);
			persona.getPokemon(pokemonElegido).getAtaques()[2]
					.setPp(persona.getPokemon(pokemonElegido).getAtaques()[2].getPp() - 1);
			ataque3Boton.setText(persona.getPokemon(pokemonElegido).getAtaques()[2].getNombre() + " | "
					+ persona.getPokemon(pokemonElegido).getAtaques()[2].getPp() + "PP");

			int danio = (int) (0.01 * ((0.2 * persona.getPokemon(pokemonElegido).getNivel() + 1)
					* persona.getPokemon(pokemonElegido).getAtaque()
					* persona.getPokemon(pokemonElegido).getAtaques()[2].getPotencia() / (25 * enemigo.getDefensa())
					+ 2));

			enemigo.setPs(enemigo.getPs() - danio);
			ataqueUsado.setText(persona.getPokemon(pokemonElegido).getNombre() + " ha usado "
					+ persona.getPokemon(pokemonElegido).getAtaques()[2].getNombre());
			danioGenerado.setText("Ha hecho " + danio + " de daño al " + enemigo.getNombre() + " enemigo.");
			vidaPokemonEnemigo.setText("PS: " + enemigo.getPs() + "/" + enemigo.getPsOriginal());
			/* Hacemos que el enemigo ataque */
			if (enemigo.getPs() <= 0) {
				vidaPokemonEnemigo.setText("PS: 0/" + enemigo.getPsOriginal());
				// clip.start();
				JOptionPane.showMessageDialog(frmPokemonLion, "El " + enemigo.getNombre() + " se ha debilitado!");
				panelLucha.setVisible(false);
				panelCombate.setVisible(false);
				menuPrincipal.setVisible(true);
				funciones.OcultarMostrarPaneles.ocultarMostrarPanel(menuPrincipalPanel);
			} else {
				funciones.Combate.ataqueEnemigo(ataque1Boton, ataque2Boton, ataque3Boton, ataque4Boton, enemigo,
						persona, ataqueUsado, danioGenerado, pokemonElegido, vidaPokemonAliado, frmPokemonLion,
						panelCombate, panelCombate, menuPrincipal, menuPrincipalPanel);
			}
		} else {
			ataque3Boton.setEnabled(false);
		}
	}

	public static void boton4(Personaje persona, int pokemonElegido, JButton ataque1Boton, Enemigo enemigo,
			JLabel ataqueUsado, JLabel danioGenerado, JLabel vidaPokemonEnemigo, JFrame frmPokemonLion,
			JPanel panelLucha, JPanel panelCombate, JPanel menuPrincipal, JButton ataque2Boton, JButton ataque3Boton,
			JButton ataque4Boton, JLabel vidaPokemonAliado, JPanel menuPrincipalPanel) {
		if (persona.getPokemon(pokemonElegido).getAtaques()[3].getPp() > 0) {
			ataque4Boton.setEnabled(true);
			persona.getPokemon(pokemonElegido).getAtaques()[3]
					.setPp(persona.getPokemon(pokemonElegido).getAtaques()[3].getPp() - 1);
			ataque4Boton.setText(persona.getPokemon(pokemonElegido).getAtaques()[3].getNombre() + " | "
					+ persona.getPokemon(pokemonElegido).getAtaques()[3].getPp() + "PP");

			int danio = (int) (0.01 * ((0.2 * persona.getPokemon(pokemonElegido).getNivel() + 1)
					* persona.getPokemon(pokemonElegido).getAtaque()
					* persona.getPokemon(pokemonElegido).getAtaques()[3].getPotencia() / (25 * enemigo.getDefensa())
					+ 2));

			enemigo.setPs(enemigo.getPs() - danio);
			ataqueUsado.setText(persona.getPokemon(pokemonElegido).getNombre() + " ha usado "
					+ persona.getPokemon(pokemonElegido).getAtaques()[3].getNombre());
			danioGenerado.setText("Ha hecho " + danio + " de daño al " + enemigo.getNombre() + " enemigo.");
			vidaPokemonEnemigo.setText("PS: " + enemigo.getPs() + "/" + enemigo.getPsOriginal());
			/* Hacemos que el enemigo ataque */
			if (enemigo.getPs() <= 0) {
				vidaPokemonEnemigo.setText("PS: 0/" + enemigo.getPsOriginal());
				// clip.start();
				JOptionPane.showMessageDialog(frmPokemonLion, "El " + enemigo.getNombre() + " se ha debilitado!");
				panelLucha.setVisible(false);
				panelCombate.setVisible(false);
				menuPrincipal.setVisible(true);
				funciones.OcultarMostrarPaneles.ocultarMostrarPanel(menuPrincipalPanel);
			} else {
				funciones.Combate.ataqueEnemigo(ataque1Boton, ataque2Boton, ataque3Boton, ataque4Boton, enemigo,
						persona, ataqueUsado, danioGenerado, pokemonElegido, vidaPokemonAliado, frmPokemonLion,
						panelCombate, panelCombate, menuPrincipal, menuPrincipalPanel);
			}
		} else {
			ataque4Boton.setEnabled(false);
		}

	}
}
