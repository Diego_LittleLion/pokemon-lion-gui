package funciones;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import lion.BolsilloObjetoLlenoExcepcion;
import lion.Objetos;
import lion.Personaje;

public class Tienda {
	
	/**
	 * Funcion para comprar en la tienda
	 * @param cantidadPokeballComprar - Indica la cantidad de pokeball que quiere comprar el jugador
	 * @param precio - Precio final que debe pagar el jugador
	 * @param persona - objeto persona, es decir, el jugador. Lo usaremos para restarle el dinero, añadirle las pokeballs...
	 * @param pokeball - Objeto pokeball, será lo que le añadiremos al jugador
	 * @param frmPokemonLion - Frame, por si tenemos que hacer un cartel emergente
	 * @param cantidadCoinsMini - Dinero del jugador, aparece en la parte infererior derecha de la interfaz del jugador
	 * @param cantidadPokeballsMini - Cantidad de pokeballs que tiene el jugador, aparece en la parte infererior derecha de la interfaz del jugador
	 * @param avisoCompraRealizada - JLabel que indicará si se ha realizado la compra o no
	 */
	public static void comprar(int cantidadPokeballComprar, int precio, Personaje persona, Objetos pokeball,
			JFrame frmPokemonLion, JLabel cantidadCoinsMini, JLabel cantidadPokeballsMini,
			JLabel avisoCompraRealizada) {
		if (persona.getPokecoins() >= precio) {
			if (persona.getCantidadObjetos() < 50) {
				try {
					persona.aniadirObjeto(pokeball, cantidadPokeballComprar);
				} catch (BolsilloObjetoLlenoExcepcion a) {
					JOptionPane.showMessageDialog(frmPokemonLion, a.toString());
				}
				persona.setPokecoins(persona.getPokecoins() - precio);
				cantidadCoinsMini.setText(Integer.toString(persona.getPokecoins()));
				cantidadPokeballsMini.setText(Integer.toString(persona.getCantidadPokeballs()));
				avisoCompraRealizada.setText("Compra realizada! Te quedan " + persona.getPokecoins() + " Pokecoins");
			} else {
				avisoCompraRealizada.setText(
						"No tienes espacios suficientes para guardar las PokeBalls, el tamaño del bolsillo es de "
								+ persona.getCantidadObjetos() + " espacios.");
			}
		} else {
			avisoCompraRealizada.setText("No tienes suficiente dinero, el precio de " + cantidadPokeballComprar
					+ " Pokeballs es de " + precio);
		}
	}

}
