package funciones;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import lion.Main;
import lion.Personaje;

public class InformacionJugador {

	public static void mostrar(JLabel mostrarNombreJL, JLabel mostrarNacimientoJL, JLabel mostrarGeneroJL, JLabel mostrarPokecoinsJL,
			JLabel mostrarCantidadObjetosJL, JLabel mostrarCantidadPokeballsJL, JLabel mostrarCantidadPokemonJL,
			Personaje persona, JLabel iInformacinDelJugadorTitulo, JLabel fotoJugador) {

		mostrarNombreJL.setText("Nombre: ");
		mostrarNacimientoJL.setText("Fecha de nacimiento: ");
		mostrarGeneroJL.setText("Genero: ");
		mostrarPokecoinsJL.setText("PokeCoins: ");
		mostrarCantidadObjetosJL.setText("Cantidad de objetos: ");
		mostrarCantidadPokeballsJL.setText("Pokeballs: ");
		mostrarCantidadPokemonJL.setText("Cantidad de Pokemon: ");
		iInformacinDelJugadorTitulo.setText("Informacion sobre ");

		iInformacinDelJugadorTitulo.setText(iInformacinDelJugadorTitulo.getText() + persona.getNombre());
		mostrarNombreJL.setText(mostrarNombreJL.getText() + persona.getNombre());
		mostrarNacimientoJL.setText(mostrarNacimientoJL.getText() + persona.getFecha_nacimiento());
		mostrarGeneroJL.setText(mostrarGeneroJL.getText() + persona.getGeneroPersonaje());
		mostrarPokecoinsJL.setText(mostrarPokecoinsJL.getText() + persona.getPokecoins());
		mostrarCantidadObjetosJL.setText(mostrarCantidadObjetosJL.getText() + persona.getCantidadObjetos());
		mostrarCantidadPokeballsJL.setText(mostrarCantidadPokeballsJL.getText() + persona.getCantidadPokeballs());
		mostrarCantidadPokemonJL.setText(mostrarCantidadPokemonJL.getText() + persona.getCantidadPokemons());

		if (persona.getGeneroPersonaje().equals("Chico")) {
			fotoJugador.setIcon(new ImageIcon(Main.class.getResource("/imagenes/chico.png")));
		} else if (persona.getGeneroPersonaje().equals("Chica")) {
			fotoJugador.setIcon(new ImageIcon(Main.class.getResource("/imagenes/chica.png")));
		} else {
			fotoJugador.setIcon(new ImageIcon(Main.class.getResource("/imagenes/interrogacion.png")));
		}
	}

}
