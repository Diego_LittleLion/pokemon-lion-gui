package funciones;

import javax.swing.JPanel;

public class OcultarMostrarPaneles {
	static JPanel panelActual;

	/**
	 * Oculta y muestra un panel
	 * 
	 * @param mostrar - Panel que se va a mostrar
	 * 
	 */
	public static void ocultarMostrarPanel(JPanel mostrar) {
		try {
			panelActual.setVisible(false);
		} catch (Exception e) {
		}
		panelActual = mostrar;
		mostrar.setVisible(true);
	}
}
