package funciones;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import lion.Main;
import lion.Personaje;

public class MostrarPokemon {

	/**
	 * Esta función muestra los datos del pokemon elegido por el jugador
	 * @param pokemonElegido
	 * @param especieConsultar
	 * @param nivelEspecieJL
	 * @param tipoEspecieJL
	 * @param psEspecieJL
	 * @param ataqueEspecieJL
	 * @param defensaEspecieJL
	 * @param capturadoEspecieJL
	 * @param nombrePokemonElegidoJL
	 * @param persona
	 * @param fotoPokemonInformacion
	 * @param ataque1JL
	 * @param ataque2JL
	 * @param ataque3JL
	 * @param ataque4JL
	 */
	public static void mostrarUnPokemon(int pokemonElegido, String especieConsultar, JLabel nivelEspecieJL,
			JLabel tipoEspecieJL, JLabel psEspecieJL, JLabel ataqueEspecieJL, JLabel defensaEspecieJL,
			JLabel capturadoEspecieJL, JLabel nombrePokemonElegidoJL, Personaje persona, JLabel fotoPokemonInformacion,
			JLabel ataque1JL, JLabel ataque2JL, JLabel ataque3JL, JLabel ataque4JL) {
		nivelEspecieJL.setText("Nivel: ");
		tipoEspecieJL.setText("Tipo: ");
		psEspecieJL.setText("PS: ");
		ataqueEspecieJL.setText("Ataque: ");
		defensaEspecieJL.setText("Defensa: ");
		capturadoEspecieJL.setText("Capturado: ");
		ataque1JL.setText("Ataque 1:  ");
		ataque2JL.setText("Ataque 2:  ");
		ataque3JL.setText("Ataque 3:  ");
		ataque4JL.setText("Ataque 4:  ");

		/* MOSTRAMOS LOS DATOS DEL POKEMON */
		
		/* Si el nombre del pokemon es igual que la especie solo mostramos el nombre del pokemon... */
		if(persona.getPokemon(pokemonElegido).getNombre().equals(persona.getPokemon(pokemonElegido).getClass().getSimpleName())) {
			nombrePokemonElegidoJL.setText(persona.getPokemon(pokemonElegido).getNombre());
		}else {
			nombrePokemonElegidoJL.setText(persona.getPokemon(pokemonElegido).getNombre() + " | "
					+ persona.getPokemon(pokemonElegido).getClass().getSimpleName());
		}
		
		nivelEspecieJL.setText(nivelEspecieJL.getText() + persona.getPokemon(pokemonElegido).getNivel());

		if (persona.getPokemon(pokemonElegido).getTipo().length == 1) {
			tipoEspecieJL.setText(tipoEspecieJL.getText() + persona.getPokemon(pokemonElegido).getTipo()[0]);
		} else {
			tipoEspecieJL.setText(tipoEspecieJL.getText() + persona.getPokemon(pokemonElegido).getTipo()[0] + " | "
					+ persona.getPokemon(pokemonElegido).getTipo()[1]);
		}

		psEspecieJL.setText(psEspecieJL.getText() + persona.getPokemon(pokemonElegido).getPs());
		ataqueEspecieJL.setText(ataqueEspecieJL.getText() + persona.getPokemon(pokemonElegido).getAtaque());
		defensaEspecieJL.setText(defensaEspecieJL.getText() + persona.getPokemon(pokemonElegido).getDefensa());
		
		ataque1JL.setText(ataque1JL.getText() + persona.getPokemon(pokemonElegido).getAtaques()[0].getNombre() + " | "
				+ persona.getPokemon(pokemonElegido).getAtaques()[0].getPp() + "PP | "
				+ persona.getPokemon(pokemonElegido).getAtaques()[0].getTipo());

		ataque2JL.setText(ataque2JL.getText() + persona.getPokemon(pokemonElegido).getAtaques()[1].getNombre() + " | "
				+ persona.getPokemon(pokemonElegido).getAtaques()[1].getPp() + "PP | "
				+ persona.getPokemon(pokemonElegido).getAtaques()[1].getTipo());

		ataque3JL.setText(ataque3JL.getText() + persona.getPokemon(pokemonElegido).getAtaques()[2].getNombre() + " | "
				+ persona.getPokemon(pokemonElegido).getAtaques()[2].getPp() + "PP | "
				+ persona.getPokemon(pokemonElegido).getAtaques()[2].getTipo());

		ataque4JL.setText(ataque4JL.getText() + persona.getPokemon(pokemonElegido).getAtaques()[3].getNombre() + " | "
				+ persona.getPokemon(pokemonElegido).getAtaques()[3].getPp() + "PP | "
				+ persona.getPokemon(pokemonElegido).getAtaques()[3].getTipo());

		capturadoEspecieJL
				.setText(capturadoEspecieJL.getText() + persona.getPokemon(pokemonElegido).getMomentoCaptura());
		fotoPokemonInformacion
				.setIcon(new ImageIcon(Main.class.getResource("/imagenesPokemon/" + especieConsultar + ".gif")));
	}

	/**
	 * Esta función rellena el box con todos los pokemon del jugador
	 * @param persona
	 * @param frmPokemonLion
	 * @param listadoPokemonBOX
	 */
	public static void listarPokemon(Personaje persona, JFrame frmPokemonLion, JComboBox listadoPokemonBOX) {
		if (persona.getCantidadPokemons() == 0) {
			JOptionPane.showMessageDialog(frmPokemonLion, "No tienes Pokemons.");
		} else {
			listadoPokemonBOX.removeAllItems();
			for (int i = 0; i < persona.getCantidadPokemons(); i++) {
				listadoPokemonBOX.addItem((i + 1) + ".-  " + persona.getPokemon(i).getNombre() + " ("
						+ persona.getPokemon(i).getClass().getSimpleName() + ") - lvl "
						+ persona.getPokemon(i).getNivel());
			}
		}
	}
}
