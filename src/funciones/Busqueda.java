package funciones;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import lion.Main;
import lion.Personaje;

public class Busqueda {

	public static void opciones(int pasosDados, int pasosRandom, JFrame frmPokemonLion, String especie, int nivel,
			Personaje persona, int especieRandom, JPanel mostrarPokemonsPanel, JPanel capturadoPanel,
			JPanel informacionPanel, JPanel panelDetalles, JPanel busquedaPanel, JPanel tiendaPanel,
			JPanel pokemonLiberadoPanel, JButton capturarPokemonBoton, JLabel escapadoJL, JLabel reintentarJL,
			JLabel capturadoJL, JLabel recompensaJL, JButton capturarBoton, JLabel pokemonAparecidoJL,
			JLabel fotoPokemonAparecido, JPanel menuPrincipal, JLabel seleccionaUnPokemonJL,
			JComboBox listaPokemonLuchar, JLabel nivelPKElegidoLucha, JLabel tipoPKElegidoLucha,
			JLabel psPKElegidoLucha, JLabel ataquePKElegidoLucha, JLabel defensaPKElegidoLucha,
			JLabel ataque1pkElegidoLucha, JLabel ataque2pkElegidoLucha, JLabel ataque3pkElegidoLucha,
			JLabel ataque4pkElegidoLucha, JLabel imgPokemonLuchar, JButton botonLuchar, JPanel capturaPanel,
			JPanel panelCombate, JPanel panelSeleccionPokemon) {
		Object[] botonesCapturaOCombate = { "Capturar", "Luchar", "Huir!" };
		if (pasosDados == pasosRandom) {
			int input = JOptionPane.showOptionDialog(frmPokemonLion,
					"Ha aparecido un " + especie + " salvaje de nivel " + nivel + "! Que quieres hacer? ",
					"Pokemon salvaje ha aparecido!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
					botonesCapturaOCombate, botonesCapturaOCombate[0]);
			/* Captura */
			if (input == 0) {
				if (persona.getCantidadPokeballs() == 0) {
					JOptionPane.showMessageDialog(frmPokemonLion, "No tienes Pokeballs.");
					especie = funciones.GenerarNivelEspecie.generarEspecie();
					nivel = funciones.GenerarNivelEspecie.generarNivel();
					pasosDados = 0;
					pasosRandom = (int) (Math.random() * (120 - 5) + 5);
				} else {
					funciones.OcultarMostrarPaneles.ocultarMostrarPanel(capturaPanel);
					capturarPokemonBoton.setVisible(true);
					capturarPokemonBoton.setText("Capturar");
					escapadoJL.setText("");
					reintentarJL.setText("");
					capturadoJL.setText("");
					recompensaJL.setText("");
					fotoPokemonAparecido
							.setIcon(new ImageIcon(Main.class.getResource("/imagenesPokemon/" + especie + ".gif")));
					pokemonAparecidoJL.setText("Ha aparecido un " + especie + " de nivel " + nivel + "!");
					capturarPokemonBoton.setVisible(true);
				}
				/* Combate */
			} else if (input == 1) {
				if (persona.getCantidadPokemons() == 0) {
					JOptionPane.showMessageDialog(frmPokemonLion, "No tienes Pokemons para luchar");
				} else {
					menuPrincipal.setVisible(false); // Ocultamos el panel principal por completo
					funciones.OcultarMostrarPaneles.ocultarMostrarPanel(panelCombate);
					panelSeleccionPokemon.setVisible(true);
					funciones.Combate.elegirPokemon(seleccionaUnPokemonJL, especie, listaPokemonLuchar, persona, nivel,
							nivelPKElegidoLucha, tipoPKElegidoLucha, psPKElegidoLucha, ataquePKElegidoLucha,
							defensaPKElegidoLucha, ataque1pkElegidoLucha, ataque2pkElegidoLucha, ataque3pkElegidoLucha,
							ataque4pkElegidoLucha, imgPokemonLuchar, botonLuchar);
				}
				/* Huir */
			} else {
				/*
				 * Si le damos a huir o no tenemos pokemon para luchar y andamos seguirán
				 * aumentando los pasos, pero el if del main que llama a esta funcion
				 * restablecerá los pasosDados a 0, generara un nuevo pokemon y un nuevo numero
				 * de pasos Random
				 */
			}
		}
	}
}
