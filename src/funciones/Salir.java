package funciones;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Salir {
	
	/**
	 * Funcion para salir del juego
	 * @param frmPokemonLion - Lo usamos para mostrar las opciones en una ventana emerjente
	 */
	public static void despedida(JFrame frmPokemonLion) {
		ImageIcon icon = new ImageIcon("src/imagenes/manoDespedida.png");
		Object[] opcionesSalir = { "Si", "No" };
		int input = JOptionPane.showOptionDialog(frmPokemonLion,
				"Estas seguro de que quieres salir?",
				"Salir", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
				icon, opcionesSalir, opcionesSalir[0]);
		
		if(input == 0) {
			System.exit(0);
		}
	}

}
