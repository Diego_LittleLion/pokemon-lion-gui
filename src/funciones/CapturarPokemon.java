package funciones;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import lion.BolsilloPokemonLlenoExcepcion;
import lion.Main;
import lion.Personaje;
import pokemon.*;

public class CapturarPokemon {

	public static void capturando(float posibilidadCaptura, JFrame frmPokemonLion, JLabel escapadoJL,
			JLabel reintentarJL, JLabel capturadoJL, Personaje persona, JLabel recompensaJL,
			JButton informacionPersonalBoton, JButton listarPokemonBoton, JButton capturarBoton, JButton tiendaBoton,
			JLabel cantidadCoinsMini, JLabel guardadoJL, JTextField ingresarMoteJF, JLabel fotoPokemonCapturado,
			JButton guardarBoton, JButton capturarPokemonBoton, String especie, JButton siguienteBoton,
			JLabel pokemonAparecidoJL) {

		if (posibilidadCaptura <= 0.33) {
			escapadoJL.setText("");
			reintentarJL.setText("");
			capturadoJL.setText("Has capturado al " + especie + "!");
			persona.setPokecoins(persona.getPokecoins() + 300);
			recompensaJL.setText("Has conseguido 300 PokeCoins!");
			/*
			 * Deshabilitamos los botones del menu para obligar al usuario a poner un mote
			 */
			informacionPersonalBoton.setEnabled(false);
			listarPokemonBoton.setEnabled(false);
			capturarBoton.setEnabled(false);
			tiendaBoton.setEnabled(false);

			cantidadCoinsMini.setText(Integer.toString(persona.getPokecoins()));
			guardadoJL.setText("");
			ingresarMoteJF.setText("");
			fotoPokemonCapturado.setIcon(new ImageIcon(Main.class.getResource("/imagenesPokemon/" + especie + ".gif")));
			guardarBoton.setEnabled(true);
			capturarPokemonBoton.setVisible(false);
			siguienteBoton.setVisible(true);
		} else {
			capturadoJL.setText("");
			recompensaJL.setText("");
			escapadoJL.setText("Oh noo! El " + especie + " ha escapado de la Pokeball!");
			reintentarJL
					.setText("Quieres intentarlo de nuevo? Tienes " + persona.getCantidadPokeballs() + " Pokeballs");
			capturarPokemonBoton.setText("Reintentar");
		}

	}

	public static void guardar(String alias, int nivel, String especie, JLabel guardadoJL, JButton guardarBoton,
			Personaje persona, JButton informacionPersonalBoton, JButton listarPokemonBoton, JButton capturarBoton,
			JButton tiendaBoton, JFrame frmPokemonLion) {
		Caterpie cat;
		Lickitung lick;
		Pidgey pid;
		Pikachu pika;
		Rayquaza ray;
		Squirtle squirt;
		try {
			if (especie.equals("Pikachu")) {
				if (alias.equals("")) {
					alias = "Pikachu";
				}
				pika = new Pikachu(alias, nivel);
				persona.aniadirPokemon(pika);
				guardarBoton.setVisible(false);
				guardadoJL.setText("Pokemon guardado");
			} else if (especie.equals("Squirtle")) {
				if (alias.equals("")) {
					alias = "Squirtle";
				}
				squirt = new Squirtle(alias, nivel);
				persona.aniadirPokemon(squirt);
				guardarBoton.setVisible(false);
				guardadoJL.setText("Pokemon guardado");
			} else if (especie.equals("Pidgey")) {
				if (alias.equals("")) {
					alias = "Pidgey";
				}
				pid = new Pidgey(alias, nivel);
				persona.aniadirPokemon(pid);
				guardarBoton.setVisible(false);
				guardadoJL.setText("Pokemon guardado");
			} else if (especie.equals("Caterpie")) {
				if (alias.equals("")) {
					alias = "Caterpie";
				}
				cat = new Caterpie(alias, nivel);
				persona.aniadirPokemon(cat);
				guardarBoton.setVisible(false);
				guardadoJL.setText("Pokemon guardado");
			} else if (especie.equals("Rayquaza")) {
				if (alias.equals("")) {
					alias = "Rayquaza";
				}
				ray = new Rayquaza(alias, nivel);
				persona.aniadirPokemon(ray);
				guardarBoton.setVisible(false);
				guardadoJL.setText("Pokemon guardado");
			} else if(especie.equals("Lickitung")){
				if (alias.equals("")) {
					alias = "Lickitung";
				}
				lick = new Lickitung(alias, nivel);
				persona.aniadirPokemon(lick);
				guardarBoton.setVisible(false);
				guardadoJL.setText("Pokemon guardado");
			}
			informacionPersonalBoton.setEnabled(true);
			listarPokemonBoton.setEnabled(true);
			capturarBoton.setEnabled(true);
			tiendaBoton.setEnabled(true);
		} catch (BolsilloPokemonLlenoExcepcion e) {
			JOptionPane.showMessageDialog(frmPokemonLion, "El bolsillo de los Pokemon está lleno.");
		}

	}
}
