package funciones;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Bienvenida {
	private static int contador = 1;

	/**
	 * Imprime los mensajes de bienvenida
	 * 
	 * @param mensajeBienvenidaJL    Etiqueta que se irá actualizando
	 * @param panelBienvenida        Panel que almacena todos los componentes
	 *                               relacionados con la intro del juego
	 * @param formularioDatosJugador formulario que deberá rellenar el jugador
	 * @param imagenOak              Foto de Oak
	 * @param menuPrincipal          Panel principal, se mostrará cuando termine la
	 *                               intro
	 * @param continuarBoton         boton de continuar, sirve para avanzar en la
	 *                               intro
	 * @param menuPrincipalPanel     Panel principal, será el que se muestre cuando
	 *                               termine la intro, contiene el menuPrincipal
	 */
	public static void imprimirBienvenida(JLabel mensajeBienvenidaJL, JPanel panelBienvenida,
			JPanel formularioDatosJugador, JLabel imagenOak, JPanel menuPrincipal, JButton continuarBoton,
			JPanel menuPrincipalPanel) {
		contador++;
		switch (contador) {
		case 1:
			mensajeBienvenidaJL.setText("Hola, encantado de conocerte!");
			break;

		case 2:
			mensajeBienvenidaJL.setText("Estas en el mundo de los pokemon!");
			break;

		case 3:
			mensajeBienvenidaJL.setText("Me llamo Oak, pero me llaman profesor Pokemon");
			break;

		case 4:
			mensajeBienvenidaJL.setText("Puedo hablarte de todo lo que necesites saber de este mundo");
			break;

		case 5:
			mensajeBienvenidaJL.setText("Este mundo esta habitado por criatura llamadas Pokemom");
			break;

		case 6:
			mensajeBienvenidaJL.setText("Los humanos y los Pokemon trabajamos juntos como amigos");
			break;

		case 7:
			mensajeBienvenidaJL.setText("Hay quien combate con sus Pokemon para estrechar lazos con ellos");
			break;

		case 8:
			mensajeBienvenidaJL.setText("Yo me dedico a estudiar los Pokemon para conocerlos mejor");
			break;

		case 9:
			mensajeBienvenidaJL.setText("Bueno, que te parece si ahora me cuentas algo sobre ti?");
			break;

		case 10:
			mensajeBienvenidaJL.setVisible(false);
			continuarBoton.setVisible(false);
			formularioDatosJugador.setVisible(true);
			break;

		case 11:
			mensajeBienvenidaJL.setText("Ha llegado el momento...");
			break;

		case 12:
			mensajeBienvenidaJL.setText("Tu propia aventura está apunto de comenzar!");
			break;

		case 13:
			mensajeBienvenidaJL.setText("Viviras aventuras emocionantes");
			break;

		case 14:
			mensajeBienvenidaJL.setText("Y pasaras momentos dificiles");
			break;

		case 15:
			mensajeBienvenidaJL.setText("Ahora...");
			break;

		case 16:
			mensajeBienvenidaJL.setText("Entra en el mundo Pokemon!");
			continuarBoton.setText("Vamos!");
			break;

		case 17:
			panelBienvenida.setVisible(false);
			menuPrincipal.setVisible(true);
			funciones.OcultarMostrarPaneles.ocultarMostrarPanel(menuPrincipalPanel);
			break;

		}
	}

}
