package funciones;

public class GenerarNivelEspecie {

	/**
	 * Esta función genera un nivel aleatorio comprendido entre el 1 y 100
	 * 
	 * @return Devuelve el nivel generado
	 */
	public static int generarNivel() {
		int nivel = (int) (Math.random() * (100 - 1) + 1);
		return nivel;
	}

	/**
	 * Esta función genera una especie aleatoria a partir de un número random
	 * generado entre el 0 y el numero de pokemons que hay en el juego
	 * 
	 * @return Devuelve la especie generada
	 */
	public static String generarEspecie() {
		int especieRandom = (int) (Math.random() * 6);
		String especie;
		if (especieRandom == 1) {
			especie = "Pikachu";
		} else if (especieRandom == 2) {
			especie = "Squirtle";
		} else if (especieRandom == 3) {
			especie = "Pidgey";
		} else if (especieRandom == 4) {
			especie = "Caterpie";
		} else if (especieRandom == 5) {
			especie = "Rayquaza";
		} else {
			especie = "Lickitung";
		}
		return especie;
	}
}
