package ataques;

public class TajoAereo extends Ataque{
	public TajoAereo() {
		this.nombre = "Tajo Aereo";
		this.categoria = "Especial";
		this.tipo = "Volador";
		this.pp = 15;
		this.ppOriginal = 15;
		this.potencia = 75;
	}
}
