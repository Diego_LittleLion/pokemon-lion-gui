package ataques;

public class Vendaval extends Ataque{
	public Vendaval() {
		this.nombre = "Vendaval";
		this.categoria = "Fisico";
		this.tipo = "Volador";
		this.pp = 10;
		this.ppOriginal = 10;
		this.potencia = 110;
	}
}
