package ataques;

public class Triturar extends Ataque{
	public Triturar() {
		this.nombre = "Triturar";
		this.categoria = "Especial";
		this.tipo = "Siniestro";
		this.pp = 15;
		this.ppOriginal = 15;
		this.potencia = 80;
	}
}
