package ataques;

public class PulsoDragon extends Ataque{
	public PulsoDragon() {
		this.nombre = "Pulso Dragon";
		this.categoria = "Especial";
		this.tipo = "Dragon";
		this.pp = 10;
		this.ppOriginal = 10;
		this.potencia = 85;
	}
}
