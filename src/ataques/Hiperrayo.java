package ataques;

public class Hiperrayo extends Ataque{
	public Hiperrayo() {
		this.nombre = "Hiperrayo";
		this.categoria = "Fisico";
		this.tipo = "Normal";
		this.pp = 5;
		this.ppOriginal = 5;
		this.potencia = 150;
	}
}
