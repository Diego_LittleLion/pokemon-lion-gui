package ataques;

public class Ataque {
	protected String nombre;
	protected String categoria;
	protected String tipo;
	protected int pp;
	protected int ppOriginal;
	protected int potencia;
	
	public int getPp() {
		return pp;
	}
	
	public int getPpOriginal() {
		return ppOriginal;
	}
	
	public void setPp(int pp) {
		this.pp = pp;
	}
	public String getNombre() {
		return nombre;
	}
	public String getCategoria() {
		return categoria;
	}
	public String getTipo() {
		return tipo;
	}
	public int getPotencia() {
		return potencia;
	}
}
