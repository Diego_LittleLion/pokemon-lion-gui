package ataques;

public class Picadura extends Ataque{
	public Picadura() {
		this.nombre = "Picadura";
		this.categoria = "Fisico";
		this.tipo = "Bicho";
		this.pp = 20;
		this.ppOriginal = 20;
		this.potencia = 60;
	}
}
