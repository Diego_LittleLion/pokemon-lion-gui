package ataques;

public class DisparoDemora extends Ataque{
	public DisparoDemora() {
		this.nombre = "Disparo Demora";
		this.categoria = "Fisico";
		this.tipo = "Bicho";
		this.pp = 40;
		this.ppOriginal = 40;
		this.potencia = 0;
	}
}
