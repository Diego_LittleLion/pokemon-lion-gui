package ataques;

public class Placaje extends Ataque{
	public Placaje() {
		this.nombre = "Placaje";
		this.categoria = "Fisico";
		this.tipo = "Normal";
		this.pp = 35;
		this.ppOriginal = 35;
		this.potencia = 40;
	}
}
