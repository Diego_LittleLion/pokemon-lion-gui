package ataques;

public class NoAtaque extends Ataque{
	public NoAtaque() {
		this.nombre = "Sin ataque";
		this.categoria = "";
		this.tipo = "";
		this.pp = 0;
		this.ppOriginal = 0;
		this.potencia = 0;
	}
}
