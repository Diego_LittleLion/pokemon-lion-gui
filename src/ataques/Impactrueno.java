package ataques;

public class Impactrueno extends Ataque{
	public Impactrueno() {
		this.nombre = "Impactrueno";
		this.categoria = "Especial";
		this.tipo = "Electrico";
		this.pp = 30;
		this.ppOriginal = 30;
		this.potencia = 40;
	}
}
