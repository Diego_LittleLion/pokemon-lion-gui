package ataques;

public class Lenguetazo extends Ataque{
	public Lenguetazo() {
		this.nombre = "Lenguetazo";
		this.categoria = "Fisico";
		this.tipo = "Fantasma";
		this.pp = 30;
		this.ppOriginal = 30;
		this.potencia = 30;
	}
}
