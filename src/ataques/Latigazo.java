package ataques;

public class Latigazo extends Ataque{
	public Latigazo() {
		this.nombre = "Latigazo";
		this.categoria = "Especial";
		this.tipo = "Planta";
		this.pp = 10;
		this.ppOriginal = 10;
		this.potencia = 120;
	}
}
