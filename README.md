POKEMON LION GUI
================

Descripción del proyecto
------------------------

Este es mi primer vídeojuego.
Esta basado en el mundo de Pokemon. En este juego deberás capturar Pokemons (como en todos los juegos),
podrás administrar tus capturas (liberar a un Pokemon o consultar sus detalles), comprar más pokeballs para seguir capturando....


Instalación
-----------

Está escrito en Java y la interfaz está hecha con Swing, el IDE usado es Eclipse aunque lo podéis abrir en netbeans importando el proyecto.
Tan solo necesitas clonar el proyecto y compilarlo, no tienes que hacer nada más :)


Controles
---------
W - Arriba
S - Abajo
A - Izquierda
D - Derecha


Copyright
---------
Todas las imagenes y sonidos pertenecen a sus respectivos creadores...


Autores
-------

DIEGO LITTLELION
Twitter: https://twitter.com/LittleFyahLion